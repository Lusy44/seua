package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * 
 * Print multiplication of all natural numbers by which the given number is
 * divided by the natural number with the remainder
 * 
 * @author lusine
 *
 */
public class Task_154 extends TaskBase {
	private static final int REQ_REMAINDER = 3;

	public static void main(String[] args) {
		Task_154 task = new Task_154();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter the number");
		System.out.println("Result = " + getProductOfFactorsNumbersWithRemainder(number));

	}

	/**
	 * Print multiplication of all natural numbers by which the given number is
	 * divided by the natural number with the remainder
	 * 
	 * @param number
	 * @return
	 */
	private double getProductOfFactorsNumbersWithRemainder(int number) {
		int multy = 1;
		for (int i = 1; i <= number - REQ_REMAINDER; i++) {
			if (isMultipleWithRemainder(number, i, REQ_REMAINDER)) {
				multy *= i;
			}
		}
		return multy;
	}
}
