package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print SUM of all the three-digit numbers that are'nt multiplied by NUMBER
 * 
 * @author lusine
 *
 */
public class Task_157 extends TaskBase {
	private static final int NUMBER = 5;

	private static final int FIRST_NUMBER = 100;
	private static final int LAST_NUMBER = 999;

	public static void main(String[] args) {
		Task_157 task = new Task_157();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		System.out.println("Result = " + getSumOfNonMultiples());

	}

	/**
	 * Return SUM of all the three-digit numbers that aren't multiplied by NUMBER
	 * 
	 * @param REQ_VALUE
	 * @return
	 */
	protected int getSumOfNonMultiples() {
		int sum = 0;
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {

			if (isNotMultiple(i, NUMBER)) {
				sum += i;
			}
		}
		return sum;
	}
}
