package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the multiplication of all natural numbers by which a given number is
 * divided without a remainder
 * 
 * @author lusine
 *
 */
public class Task_152 extends TaskBase {

	public static void main(String[] args) {
		Task_152 task = new Task_152();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter the number");
		System.out.println("Result = " + getProductOfFactorsNumbers(number));

	}

	/**
	 * Return the multiplication of all natural numbers by which a given number is
	 * divided without a remainder
	 * 
	 * @param number
	 * @return
	 */
	private double getProductOfFactorsNumbers(int number) {
		int multy = number;
		for (int i = 1; i <= number / 2; i++) {
			if (isMultiple(number, i)) {
				multy *= i;
			}
		}
		return multy;
	}
}
