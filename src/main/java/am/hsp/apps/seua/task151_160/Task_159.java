package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the multiplication all the three-digit numbers that are multiples of
 * "NUMBER_1" a remainder "Remainder_1" and the numbers that are multiples of
 * "NUMBER_2" a remainder "Remainder_2"
 * 
 * @author lusine
 *
 */
public class Task_159 extends TaskBase {
	private static final int NUMBER_1 = 3;
	private static final int NUMBER_2 = 4;

	private static final int REMAINDER_1 = 1;
	private static final int REMAINDER_2 = 2;

	private static final int FIRST_NUMBER = 100;
	private static final int LAST_NUMBER = 999;

	public static void main(String[] args) {
		Task_159 task = new Task_159();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Result = " + getResult());

	}

	/**
	 * Return the multiplication all the three-digit numbers that are multiples of
	 * "NUMBER_1" a remainder "Remainder1" and the numbers that are multiples of
	 * "NUMBER_2" a remainder "Remainder2"
	 * 
	 * @param number
	 * @return
	 */
	private long getResult() {
		long product = 1;
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {
			if ((isMultipleWithRemainder(i, NUMBER_1, REMAINDER_1))
					&& (isMultipleWithRemainder(i, NUMBER_2, REMAINDER_2))) {
				product *= i;
			}
		}

		return product;

	}
}
