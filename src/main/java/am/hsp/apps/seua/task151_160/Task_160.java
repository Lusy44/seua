package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the smallest three digit number, which multiplied number equal natural
 * number, otherwise return "-2"
 * 
 * @author lusine
 *
 */
public class Task_160 extends TaskBase {
	private static final int FIRST_NUMBER = 100;
	private static final int LAST_NUMBER = 999;
	private static final int NUMBER = 16;
	private static final int NOT_SOLUTION = -2;

	public static void main(String[] args) {
		Task_160 task = new Task_160();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Result = " + getResult(NUMBER));

	}

	/**
	 * Return the smallest three digit number, which multiplied number equal natural
	 * number, otherwise return "-2"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {
			if (hasRoot(i * number)) {
				return i;
			}
		}
		return NOT_SOLUTION;
	}

}
