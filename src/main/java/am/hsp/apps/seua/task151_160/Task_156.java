package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the multiplication all the two-digit numbers that are multiplied by
 * "NUMBER 1" and "NUMBER 2"
 * 
 * @author lusine
 *
 */
public class Task_156 extends TaskBase {
	private static final int NUMBER_1 = 3;
	private static final int NUMBER_2 = 5;

	private static final int FIRST_NUMBER = 10;
	private static final int LAST_NUMBER = 99;

	public static void main(String[] args) {
		Task_156 task = new Task_156();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Result = " + getProductOfMultiplesForNumber());

	}

	/**
	 * Return the multiplication all the two-digit numbers that are multiplied by
	 * Number_1 and Number_2
	 * 
	 * @param number
	 * @return
	 */
	private int getProductOfMultiplesForNumber() {
		int multy = 1;
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {

			if (isMultiple(i, NUMBER_1) && isMultiple(i, NUMBER_2)) {
				multy *= i;
			}

		}
		return multy;

	}
}
