package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the multiplication all the three-digit numbers that aren't multiplied
 * by NUMBER_1 and NUMBER_2
 * 
 * @author lusine
 *
 */
public class Task_158 extends TaskBase{
	private static final int NUMBER_1 = 2;
	private static final int NUMBER_2 = 3;

	private static final int FIRST_NUMBER = 100;
	private static final int LAST_NUMBER = 999;

	public static void main(String[] args) {
		Task_158 task = new Task_158();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Result = " + getProductOfNonMultiples());

	}

	/**
	 * Return the multiplication all the three-digit numbers that aren't multiplied
	 * by NUMBER_1 and NUMBER_2
	 * 
	 * @param number
	 * @return
	 */
	private long getProductOfNonMultiples() {
		long product = 1;
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {

			if (isNotMultiple(i, NUMBER_1) && (isNotMultiple(i, NUMBER_2))) {
				product *= i;
			}
		}

		return product;

	}
}
