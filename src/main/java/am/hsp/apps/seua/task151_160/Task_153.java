package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print sum of all natural numbers by which the given number is divided by the
 * natural number with the remainder
 * 
 * 
 * @author lusine
 *
 */
public class Task_153 extends TaskBase {
	private static final int REQ_REMAINDER = 2;

	public static void main(String[] args) {
		Task_153 task = new Task_153();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the number");
		System.out.println("Result = " + getSumOfFactorsNumbersWithRemainder(number));
	}

	/**
	 * Return sum of all natural numbers by which the given number is divided by the
	 * natural number with the remainder
	 * 
	 * @param number
	 * @return
	 */
	private int getSumOfFactorsNumbersWithRemainder(int number) {
		int sum = 0;
		for (int i = 1; i <= number - REQ_REMAINDER; i++) {
			if (isMultipleWithRemainder(number, i, REQ_REMAINDER)) {
				sum += i;
			}
		}
		return sum;
	}

}
