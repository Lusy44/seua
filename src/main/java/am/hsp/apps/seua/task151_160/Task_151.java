package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the sum of all natural numbers by which a given number is divided
 * without a remainder
 * 
 * @author lusine
 *
 */
public class Task_151 extends TaskBase {

	public static void main(String[] args) {
		Task_151 task = new Task_151();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter the number");
		System.out.println("Result = " + getSumOfFactorsNumbers(number));

	}

	/**
	 * Return the sum of all natural numbers by which a given number is divided
	 * without a remainder
	 * 
	 * @param number
	 * @return
	 */
	private int getSumOfFactorsNumbers(int number) {
		int sum = number;
		for (int i = 1; i <= number / 2; i++) {
			if (isMultiple(number, i)) {
				sum += i;
			}
		}
		return sum;
	}
}
