package main.java.am.hsp.apps.seua.task151_160;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print SUM of all the two-digit numbers that are multiplied by NUMBER.
 * 
 * @author lusine
 *
 */
public class Task_155 extends TaskBase {
	private static final int NUMBER = 3;
	private static final int FIRST_NUMBER = 10;
	private static final int LAST_NUMBER = 99;

	public static void main(String[] args) {
		Task_155 task = new Task_155();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		System.out.println("Result = " + getSumOfMultiplesForNumber());

	}

	/**
	 * Return SUM of all the two-digit numbers that are multiplied by NUMBER
	 * 
	 * @param NUMBER
	 * @return
	 */
	protected int getSumOfMultiplesForNumber() {
		int sum = 0;
		for (int i = FIRST_NUMBER; i <= LAST_NUMBER; i++) {
			if (isMultiple(i, NUMBER)) {
				sum += i;
			}
		}
		return sum;
	}
}
