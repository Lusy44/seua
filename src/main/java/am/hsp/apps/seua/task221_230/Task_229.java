package main.java.am.hsp.apps.seua.task221_230;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the multiply of elements, the difference between their value and the
 * index is a positive number
 * 
 * @author lusine
 *
 */
public class Task_229 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_229 task = new Task_229();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result: " + getResult(array));

	}

	/**
	 * Return the multiply of elements, the difference between their value and the
	 * index is a positive number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getResult(int[] array) {
		int product = 1;

		for (int i = 0; i < array.length; i++) {
			if (isElmAndIndexDifferencePositive(array, i)) {
				product *= array[i];
			}
		}
		return product;
	}

	/**
	 * Check the difference between the value and the index a positive number
	 * 
	 * @param number  SUBTRACTION
	 * @param specifiedNumber
	 * @return
	 */
	private boolean isElmAndIndexDifferencePositive(int[] array, int index) {
		return (array[index] - index > 0);
	}
}
