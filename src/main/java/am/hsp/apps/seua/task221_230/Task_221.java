package main.java.am.hsp.apps.seua.task221_230;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum of elements, which from A to B :[a:b]
 * 
 * @author lusine
 *
 */
public class Task_221 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_221 task = new Task_221();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int beginRange = getUserInput("Please enter begin A of range");
		int finishRange = getUserInput("Please enter finish B of range");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println(getSumOfElementsRange(array, beginRange, finishRange));

	}

	/**
	 * Return the sum of elements, which from A to B :[a:b]
	 * 
	 * @param array
	 * @param beginRange
	 * @param endRange
	 * @return
	 */
	private int getSumOfElementsRange(int[] array, int beginRange, int endRange) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {

			if (isNumberOfRange(array[i], beginRange, endRange)) {
				sum += array[i];
			}
		}

		return sum;
	}
}
