package main.java.am.hsp.apps.seua.task221_230;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum of elements whose index divides the number without a remainder
 * 
 * @author lusine
 *
 */
public class Task_228 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_228 task = new Task_228();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result: " + getSumMultiple(array, number));

	}

	/**
	 * Return the sum of elements whose index divides the number without a remainder
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getSumMultiple(int[] array, int number) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (isMultiple(i, number)) {
				sum += array[i];
			}
		}

		return sum;
	}

}
