package main.java.am.hsp.apps.seua.task221_230;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum cubes of elements whose absolute value is less than the
 * specified number
 * 
 * @author lusine
 *
 */
public class Task_224 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_224 task = new Task_224();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result: " + getSumCubesOfComplyTheCondition(array, number));

	}

	/**
	 * Return the sum cubes of elements whose absolute value is less than the
	 * specified number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getSumCubesOfComplyTheCondition(int[] array, int number) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (isAbsLessNumber(array[i], number)) {
				sum += getPow(array[i], 3);
			}
		}
		return sum;
	}

	/**
	 * Check absolute value of number is less than the specified number
	 * 
	 * @param number
	 * @param specifiedNumber
	 * @return
	 */
	private boolean isAbsLessNumber(int number, int numberToCompareWith) {
		return (Math.abs(number) < numberToCompareWith);
	}
}
