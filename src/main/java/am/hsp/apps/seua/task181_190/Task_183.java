package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Calculate the smallest number K, whose in case BASE ^ K > N
 * 
 * @author lusine
 *
 */
public class Task_183 extends TaskBase {
	private static final int BASE = 3;

	public static void main(String[] args) {
		Task_183 task = new Task_183();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println(getSmallestDegree(number));
	}

	/**
	 * Calculate and Print the smallest number K, whose in case BASE ^ K > N
	 * 
	 * @param number
	 * @return
	 */
	private int getSmallestDegree(int number) {	
		int degree = 0;
		int tempNumber = 0;

		do {
			tempNumber = getPow(BASE, ++degree);			
		} while (tempNumber < number);
		
		return degree;
	}

}
