package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * The skier has passed WAY.Each next day it is added P Percent. Calculate when
 * the WAY will be greater than FINAL_WAY
 * 
 * @author lusine
 *
 */
public class Task_186 extends TaskBase {
	private static final double WAY = 20;
	private static final double FINISH_WAY = 80;

	public static void main(String[] args) {
		Task_186 task = new Task_186();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int percent = getUserInput("Please enter percent");

		printFinishWayAndDay(WAY, percent);
	}

	/**
	 * The skier has passed WAY.Each next day it is added P Percent. Calculate when
	 * the WAY will be greater than FINAL_WAY
	 * 
	 * @param speed
	 * @param percent
	 */
	private void printFinishWayAndDay(double way, int percent) {
		double currentWayofDay = WAY;
		int day = 1;
		double allCurrentWay = WAY;

		while (currentWayofDay <= FINISH_WAY) {
			currentWayofDay += getPercent(currentWayofDay, percent);
			allCurrentWay += currentWayofDay;
			day++;
		
		}
		
		System.out.println("Final all way = " + allCurrentWay); 
		System.out.println("Number of day = " + day);
	}
	
	/**
	 * Calculates the percentage of the given number
	 * 
	 * @param money
	 * @param percent
	 * @return
	 */
	private double getPercent(double way, int percent) {
		return way * percent / 100;
	}

}
