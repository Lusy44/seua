package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * In bank have a MONEY. Each month it is added P Percent. Calculate when the
 * MONEY will be greater than FINAL_MONEY
 * 
 * @author lusine
 *
 */
public class Task_185 extends TaskBase {
	private static final double FINAL_MONEY = 100000;
	private static final double MONEY = 30000;

	public static void main(String[] args) {
		Task_185 task = new Task_185();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int percent = getUserInput("Please enter percent");

		printFinalMoneyAndMonth(MONEY, percent);
	}

	/**
	 * In bank have a MONEY. Each month it is added P Percent. Calculate when the
	 * MONEY will be greater than FINAL_MONEY
	 * 
	 * @param money
	 * @param percent
	 */
	private void printFinalMoneyAndMonth(double money, int percent) {
		double currentMoney = MONEY;
		int month = 0;
		
		while (currentMoney <= FINAL_MONEY) {
			currentMoney += getPercent(currentMoney, percent);
			month ++;
		}
		 System.out.println("Final Money = " + currentMoney);
		 System.out.println("Number of months = " + month);
	}

	/**
	 * Calculates the percentage of the given number
	 * 
	 * @param money
	 * @param percent
	 * @return
	 */
	private double getPercent(double money, int percent) {
		return money * percent / 100;
	}

}
