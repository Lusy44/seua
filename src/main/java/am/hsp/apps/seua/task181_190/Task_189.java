package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print Fibonacci number, which the greater than the given number: F[n] =
 * F[n-1] + F[n-2], where is F[1] = 1, F[2] = 1;
 * 
 * @author lusine
 *
 */
public class Task_189 extends TaskBase {
	private static final int FIRST_NUMBER = 1;
	private static final int SEC_NUMBER = 1;

	public static void main(String[] args) {
		Task_189 task = new Task_189();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		printFibonacciNumberGreaterNumber(number);

	}

	/**
	 * Print Fibonacci number, which the greater than the given number: F[n] =
	 * F[n-1] + F[n-2], where is F[1] = 1,
	 * 
	 * @param number
	 * @return
	 */
	private void printFibonacciNumberGreaterNumber(int number) {
		int index = 1;
		int currentElement = FIRST_NUMBER;

		while (currentElement <= number) {
			currentElement = getFibonacciNumber(++index);
		}

		System.out.println(currentElement);
	}

}
