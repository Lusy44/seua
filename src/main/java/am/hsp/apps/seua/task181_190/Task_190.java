package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * The sequence is as follows: x[n] = (x[n-2] + 2 * x[n-1] / 3: 
 * Decide the smallest number N, in which case |x[n] - x[n-1]| < Epsilion
 * 
 * @author lusine
 *
 */
public class Task_190 extends TaskBase {
	private static final int FIRST_NUMBER = 1;
	private static final int SEC_NUMBER = 2;

	public static void main(String[] args) {
		Task_190 task = new Task_190();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int epsilion = getUserInput("Please enter number epsilion");
		System.out.println(getResult(epsilion));
	}

	/**
	 * Calculate current element with Recursion: (x[n-2] + 2 * x[n-1]) / 3;
	 * 
	 * @param index
	 * @return
	 */
	private double getCurrentElement(int index) {
		if (index == 1) {
			return FIRST_NUMBER;
		}
		if (index == 2) {
			return SEC_NUMBER;
		}

		return (getCurrentElement(index - 2) + 2 * getCurrentElement(index - 1)) / 3;
	}

	/**
	 * Decide the smallest number N, in which case |x[n] - x[n-1]| < Epsilion
	 * @param epsilion
	 * @return
	 */
	private int getResult(int epsilion) {
		int index = 1;
		double result = FIRST_NUMBER;

		while (result >= epsilion ) {
			result = getAbsValue(++index);
		}
		return index;
	}

	/**
	 * Calculate the expression value
	 * 
	 * @param index
	 * @return
	 */
	private double getAbsValue(int index) {
		double result = Math.abs(getCurrentElement(index) - getCurrentElement(index - 1));

		return result;
	}

}
