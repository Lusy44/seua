package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;
/**
 * Print "true", if the given number Prime, otherwise Print "false"
 */
public class Task_187 extends TaskBase {

	public static void main(String[] args) {
		Task_187 task = new Task_187();
		task.solve();

	}
/*
 * (non-Javadoc)
 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		
		System.out.println(isPrime(number));
	}

}
