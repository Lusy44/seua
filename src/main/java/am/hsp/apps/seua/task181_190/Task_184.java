package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Calculate the biggest number K, whose in case BASE ^ K < N
 * 
 * @author lusine
 *
 */
public class Task_184 extends TaskBase {
	private static final int BASE = 3;
	
	public static void main(String[] args) {
		Task_184 task = new Task_184();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println(getBiggestDegree(number));
	}

	/**
	 * Calculate and Print the biggest number K, whose in case BASE ^ K < N
	 * 
	 * @param number
	 * @return
	 */
	private int getBiggestDegree(int number) {
		int degree = 1;
		int result = 1;

		while (result < number) {
			result = getPow(BASE, ++degree);
		}

		return (degree - 1);
	}

}
