package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * The given number, which is degree of two... Calculate is which degree
 * 
 * @author lusine
 *
 */
public class Task_181 extends TaskBase {
	private static final int BASE = 2;

	public static void main(String[] args) {
		Task_181 task = new Task_181();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number, which will be the number of degrees");

		System.out.println("Reslut = " + getPowerOfBase(number, BASE));

	}

}
