package main.java.am.hsp.apps.seua.task181_190;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Without a square root, calculate the maximum number K, the square of which is
 * less or equal than the specified number N
 * 
 * @author lusine
 *
 */
public class Task_182 extends TaskBase {

	public static void main(String[] args) {
		Task_182 task = new Task_182();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number");

		System.out.println(getMaxBase(number));
	}

	/**
	 * Calculate the maximum number K, the square of which is less or equal than the
	 * specified number N
	 * 
	 * @param number
	 * @return
	 */
	private int getMaxBase(int number) {
		int base = 0;
		int tempNumber = 1;

		do {
			tempNumber = getPow(++base, 2);
		} while (tempNumber <= number);

		return base - 1;
	}

}
