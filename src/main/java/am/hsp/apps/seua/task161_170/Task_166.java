package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "ANSWER_2", if a given number is the power of the base, otherwise Print "ANSWER_1"
 * @author lusine
 *
 */
public class Task_166 extends TaskBase {
	private static final int BASE = 4;
	private static final int ANSWER_1 = 0;
	private static final int ANSWER_2 = 1;

	public static void main(String[] args) {
		Task_166 task = new Task_166();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please etner number");
		System.out.println("t = " + getResult(number));

	}

	/**
	 * Return "ANSWER_2", if a given number is the degree of the divisor, otherwise Return "ANSWER_1"
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		if (isAnyPowerOfBase(number, BASE)) {
			return ANSWER_2;
		}
		return ANSWER_1;
	}

}
