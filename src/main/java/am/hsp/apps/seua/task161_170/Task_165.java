package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if a given number is the power of the base, otherwise Print
 * "false"
 * 
 * @author lusine
 *
 */
public class Task_165 extends TaskBase {
	private static final int BASE = 3;

	public static void main(String[] args) {
		Task_165 task = new Task_165();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please etner number");
		System.out.println("t = " + isAnyPowerOfBase(number, BASE));

	}

}
