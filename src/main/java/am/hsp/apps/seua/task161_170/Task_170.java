package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the smallest number, which greater than "number" and the power of base
 * 
 * @author lusine
 *
 */
public class Task_170 extends TaskBase {
	private static final int BASE = 2;

	public static void main(String[] args) {
		Task_170 task = new Task_170();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		System.out.println("Result = " + getResult(number));

	}

	/**
	 * Print the smallest number, which greater than "number" and the power of base
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		int i = number + 1;
		while (!(isAnyPowerOfBase(i, BASE))) {
			i++;
		}

		return i;
	}
}
