package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the smallest four digit number in which multiplied NUMBER equal to the
 * square natural number, otherwise Print NOT_SOLUTION
 * 
 * @author lusine
 *
 */
public class Task_161 extends TaskBase {
	private static final int NUMBER_FROM = 1000;
	private static final int NUMBER_TO = 9999;
	private static final int NUMBER = 26;
	private static final int NOT_SOLUTION = -1;

	public static void main(String[] args) {
		Task_161 task = new Task_161();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Result = " + getResult(NUMBER));

	}

	/**
	 * Return the smallest four digit number, which multiplied NUMBER equal to the
	 * square natural number, otherwise NOT_SOLUTION
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		for (int i = NUMBER_FROM; i <= NUMBER_TO; i++) {
			if (hasRoot(number * i)) {
				return i;
			}
		}
		return NOT_SOLUTION;
	}

}
