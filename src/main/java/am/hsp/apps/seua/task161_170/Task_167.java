package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if the value of sin(x) degree n there is negative value,
 * otherwise Print "false"
 * 
 * @author lusine
 *
 */
public class Task_167 extends TaskBase {
	private static final int NUMBER_FROM = 1;
	private static final int NUMBER_TO = 30;

	public static void main(String[] args) {
		Task_167 task = new Task_167();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number");
		System.out.println("Result: " + isAnyValueNegative(number));

	}

	/**
	 * Return "true", if the value of sin(x) degree n there is negative value,
	 * otherwise Return "false"
	 * 
	 * @param number
	 * @return
	 */
	private boolean isAnyValueNegative(int number) {
		for (int i = NUMBER_FROM; i <= NUMBER_TO; i++) {
			if (Math.sin(getPow(number, i)) < 0) {
				return true;
			}
		}
		return false;
	}

}
