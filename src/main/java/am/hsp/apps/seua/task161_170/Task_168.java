package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if the given number is Prime, otherwise "false"
 * 
 * @author lusine
 *
 */
public class Task_168 extends TaskBase {

	public static void main(String[] args) {
		Task_168 task = new Task_168();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		System.out.println(isPrime(number));

	}

}
