package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the smallest number in which the square root is greater than the given
 * number, Otherwise Print NOT_SOLUTION;
 * 
 * @author lusine
 *
 */
public class Task_164 extends TaskBase {
	private static final int NUMBER_FROM = 100;
	private static final int NUMBER_TO = 999;
	private static final int NOT_SOLUTION = -1;

	public static void main(String[] args) {
		Task_164 task = new Task_164();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("PLease enter number");
		System.out.println("Result = " + getResult(number));

	}

	/**
	 * Return the smallest number in which the square root is greater than the given
	 * number, Otherwise Return NOT_SOLUTION;
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		for (int i = NUMBER_FROM; i <= NUMBER_TO; i++) {
			if (Math.sqrt(i) > number) {
				return i;
			}
		}
		return NOT_SOLUTION;
	}

}
