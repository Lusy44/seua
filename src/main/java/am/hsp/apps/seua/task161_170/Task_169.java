package main.java.am.hsp.apps.seua.task161_170;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print ANSWER_1, if the sum of given numbers is Prime, otherwise Print
 * ANSWER_2
 * 
 * @author lusine
 *
 */
public class Task_169 extends TaskBase {
	private static final int ANSWER_1 = 5;
	private static final int ANSWER_2 = 6;

	public static void main(String[] args) {
		Task_169 task = new Task_169();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please etner number1");
		int number2 = getUserInput("Please etner number2");
		System.out.println("z = " + getPrimeSumOfNumbers(number1, number2));
	}

	/**
	 * Return ANSWER_1, if the sum of given numbers is Prime, otherwise Return
	 * ANSWER_2
	 */
	private int getPrimeSumOfNumbers(int number1, int number2) {
		if (isPrime(number1 + number2)) {
			return ANSWER_1;
		}
		return ANSWER_2;
	}

}
