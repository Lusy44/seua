package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true" if any of the numbers is equal to Request final, otherwise Print
 * "false"
 * 
 * @author lusine
 */
public class Task_33 extends TaskBase {
	private static final int REQ_VALUE = 1;

	/*
	 * Print "true", if at least one of the numbers Equal Request Value, otherwise
	 * "false"
	 */
	public static void main(String[] args) {

		Task_33 task = new Task_33();
		task.solve();
	}

	@Override
	public void solve() {

		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println("Result: " + isAnyNumberEqualReqValue(number1, number2, number3, number4));
	}

	/**
	 * Return "true", if the number equal to Required Value, otherwise Return
	 * "false"
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	private boolean isNumberEqualToRequiredValue(int number1) {
		return (number1 == REQ_VALUE);
	}

	/**
	 * Check and Return "true", if at least one of the numbers Equal request value,
	 * otherwise "false"
	 */
	private boolean isAnyNumberEqualReqValue(int number1, int number2, int number3, int number4) {
		return (isNumberEqualToRequiredValue(number1) || isNumberEqualToRequiredValue(number2)
				|| isNumberEqualToRequiredValue(number3) || isNumberEqualToRequiredValue(number4));
	}

}
