package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Sort the given numbers in ascending order
 */
public class Task_39 extends TaskBase {

	public static void main(String[] args) {
		Task_39 task = new Task_39();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int temp = 0;
		int number1 = getUserInput("PLease, enter number1");
		int number2 = getUserInput("PLease, enter number2");
		int number3 = getUserInput("PLease, enter number3");
		int number4 = getUserInput("PLease, enter number4");

		if (number1 > number2) {
			temp = number1;
			number1 = number2;
			number2 = temp;
		}
		if (number2 > number3) {
			temp = number2;
			number2 = number3;
			number3 = temp;
		}
		if (number3 > number4) {
			temp = number3;
			number3 = number4;
			number4 = temp;
		}
		if (number1 > number2) {
			temp = number1;
			number1 = number2;
			number2 = temp;
		}
		if (number2 > number3) {
			temp = number2;
			number2 = number3;
			number3 = temp;
		}
		if (number1 > number2) {
			temp = number1;
			number1 = number2;
			number2 = temp;
		}
		System.out.println("Result " + number1 + ", " + number2 + ", " + number3 + ", " + number4);
	}

}
