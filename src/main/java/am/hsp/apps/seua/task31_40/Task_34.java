package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if the given two numbers sum equal to the two other numbers Sum, Otherwise "false"   
 */
public class Task_34 extends TaskBase {

	public static void main(String[] args) {
		Task_34 task = new Task_34();
		task.solve();

	}

	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println("Result: " + isSumOfAnyTwoEqualsToSumOfOthers(number1, number2, number3, number4));
		
	}
	 
	 /**
	  * Check and Return "true", if the any Sum Couple equal the other sum couples, otherwise Return "false":
	  * @param number1
	  * @param number2
	  * @param number3
	  * @param number4    
	  * @return
	  */
	 protected boolean isSumOfAnyTwoEqualsToSumOfOthers(int number1, int number2, int number3, int number4) {
		 return (getSumNumbers(number1, number2) == getSumNumbers(number3, number4) || 
				 getSumNumbers(number1, number3) == getSumNumbers(number2, number4) || 
				 getSumNumbers(number1, number4) == getSumNumbers(number2, number3));
	 }

}


