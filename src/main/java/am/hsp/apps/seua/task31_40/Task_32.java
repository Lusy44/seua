package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/*
 * Print minimum of given 4 numbers
 */
public class Task_32 extends TaskBase {

	public static void main(String[] args) {
		Task_32 task = new Task_32();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println("Result Min = " + getMin(number1, number2, number3, number4));

	}

}
