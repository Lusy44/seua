package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the maximum of given 4 Numbers
 */
public class Task_31 extends TaskBase {
	public static void main(String[] args) {
		Task_31 task = new Task_31();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println("Result Max = " + getMax(number1, number2, number3, number4));

	}

}
