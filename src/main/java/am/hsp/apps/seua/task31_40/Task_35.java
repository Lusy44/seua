package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if any of the given numbers equal to the three numbers sum,
 * otherwise Print "false"
 * 
 * @author Lusine
 *
 */
public class Task_35 extends TaskBase {

	public static void main(String[] args) {
		Task_35 task = new Task_35();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println("Result: = " + isAnyNumberEqualSumOtherNumbers(number1, number2, number3, number4));

	}

	/**
	 * Return "true ", if first parameter equal other numbers to the sum, otherwise
	 * Return "false
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	private static boolean isFirstNumberEqualSumThreeNumber(int number1, int number2, int number3, int number4) {
		return (number1 == number2 + number3 + number4);
	}

	/**
	 * Check and Return "true" if any of the given numbers equal to the three
	 * numbers Sum, otherwise print "false"
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	private static boolean isAnyNumberEqualSumOtherNumbers(int number1, int number2, int number3, int number4) {
		return (isFirstNumberEqualSumThreeNumber(number1, number2, number3, number4)
				|| isFirstNumberEqualSumThreeNumber(number2, number3, number1, number4)
				|| isFirstNumberEqualSumThreeNumber(number3, number2, number1, number4)
				|| isFirstNumberEqualSumThreeNumber(number4, number2, number3, number1));

	}
}
