package main.java.am.hsp.apps.seua.task31_40;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if any two numbers are odd, otherwise Print "false"
 * 
 * @author Lusine
 *
 */
public class Task_36 extends TaskBase {

	public static void main(String[] args) {
		Task_36 task = new Task_36();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");
		int number4 = getUserInput("Please enter number4");

		System.out.println(" Result = " + isAnyBothNumberOdd(number1, number2, number3, number4));

	}

	/**
	 * Check and Return "true" if the two numbers are Odd
	 * 
	 * @param number
	 * @return
	 */
	protected boolean isBothOdd(int number1, int number2) {
		return (isOdd(number1) && isOdd(number2));
	}

	/**
	 * Return " true", if any the two numbers too is odd, otherwise return "false"
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	protected boolean isAnyBothNumberOdd(int number1, int number2, int number3, int number4) {
		return (isBothOdd(number1, number2) || isBothOdd(number1, number3) || isBothOdd(number1, number4)
				|| isBothOdd(number2, number3) || isBothOdd(number2, number4) || isBothOdd(number3, number4));
	}

}
