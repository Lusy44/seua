package main.java.am.hsp.apps.seua.base;

/**
 * Contains all general methods necessary for solving digital tasks.
 * 
 * @author lusine
 *
 */
public abstract class TaskDigitBase extends TaskBase {
	private static final int TENS = 2;
	private static final int HUNDREDS = 3;
	private static final int THOUSANDS = 4;

	/**
	 * The number increases to a given degree
	 * 
	 * @param number
	 * @param index
	 * @return
	 */
	protected int getPow(int number, int degree) {
		int result = 1;
		for (int i = 1; i <= degree; i++) {
			result *= number;
		}
		return result;
	}

	/**
	 * Get Ones Digit from the given three digit number
	 * 
	 * @param number
	 * @return
	 */
	protected int getOnes(int number) {
		return number % 10;
	}

	/**
	 * Get Digit by index from the given Number
	 * 
	 * @param number
	 * @param index
	 * @return
	 */
	protected int getDigitByIndex(int number, int index) {
		return (getOnes(number / getPow(10, index - 1)));
	}

	/**
	 * Get Tens Digit from the given number
	 * 
	 * @param number
	 * @return
	 */
	protected int getTens(int number) {
		return getDigitByIndex(number, TENS);
	}

	/**
	 * Get Hundreds Digit from the given number
	 * 
	 * @param number
	 * @return
	 */
	protected int getHundreds(int number) {
		return getDigitByIndex(number, HUNDREDS);
	}

	/**
	 * Get Thousands Digit from the given number
	 * 
	 * @param number
	 * @return
	 */
	protected int getThousands(int number) {
		return getDigitByIndex(number, THOUSANDS);
	}

	/**
	 * Remove last digit from the given number
	 */
	protected int removeLastDigit(int number) {
		return number /= 10;
	}

	/**
	 * Remove two last digit from the given number
	 */
	protected int removeTwoLastDigit(int number) {
		return number /= 100;
	}

	/**
	 * Calculate count the Digits from the given number
	 * 
	 * @param number
	 * @return
	 */
	protected int getDigitCount(int number) {
		int count = 0;
		do {
			number = removeLastDigit(number);
			count++;
		} while (number > 0);

		return count;
	}

	/**
	 * Calculate the Sum of digits the given number
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	protected int getSumOfDigit(int number) {
		int sum = getOnes(number);
		number = removeLastDigit(number);

		while (number > 0) {
			int ones = getOnes(number);
			sum += ones;
			number = removeLastDigit(number);
		}
		return sum;
	}

	/**
	 * Calculate the multiplication of digits by the given number
	 */
	protected int getProductOfDigits(int number) {
		int result = getOnes(number);
		number = removeLastDigit(number);

		while (number > 0) {
			int ones = getOnes(number);
			result *= ones;
			number = removeLastDigit(number);

		}
		return result;
	}

	/**
	 * Get the maximum of the given number digits
	 * 
	 * @param number
	 */
	protected int getMaxDigit(int number) {
		int max = getOnes(number);
		number = removeLastDigit(number);

		while (number > 0) {
			int ones = getOnes(number);
			if (ones > max) {
				max = ones;
			}
			number = removeLastDigit(number);

		}
		return max;
	}

	/**
	 * Get the minimum of the given number digits
	 * 
	 * @param number
	 */
	protected int getMinDigit(int number) {
		int min = getOnes(number);
		number = removeLastDigit(number);

		while (number > 0) {
			int ones = getOnes(number);
			if (ones < min) {
				min = ones;
			}
			number = removeLastDigit(number);
		}
		return min;
	}

	/**
	 * Calculate and Return sum of even digits
	 * 
	 * @param number
	 * @return
	 */
	protected int getSumEvenDigit(int number) {
		int sumEven = getTens(number);

		while (number > 0) {
			number = removeTwoLastDigit(number);
			sumEven += getTens(number);
		}
		return sumEven;
	}

	/**
	 * Calculate and Return sum of odd digits
	 * 
	 * @param number
	 * @return
	 */
	protected int getSumOddDigit(int number) {
		int sumOdd = getOnes(number);

		while (number > 0) {
			number = removeTwoLastDigit(number);
			sumOdd += getOnes(number);
		}
		return sumOdd;
	}

}