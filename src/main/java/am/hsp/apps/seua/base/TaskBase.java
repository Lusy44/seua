package main.java.am.hsp.apps.seua.base;

import java.util.Random;
import java.util.Scanner;

/**
 * Base class for all the tasks. Will contain general methods.
 * 
 * @author lusine
 *
 */
public abstract class TaskBase {
	/**
	 * This will contain actual logic for any task ...
	 */
	private static final int NOT_FOUND = -1;

	public abstract void solve();

	/**
	 * Display message to user and return user input...
	 * 
	 * @param message
	 * @return
	 */
	protected int getUserInput(String message) {
		System.out.println(message);
		Scanner scaner = new Scanner(System.in);
		return scaner.nextInt();
	}

	/**
	 * Get the maximum of the given two number
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	protected int getMax(int number1, int number2) {
		return (number1 >= number2) ? number1 : number2;

	}

	/**
	 * Get the maximum of the given three number
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	protected int getMax(int number1, int number2, int number3) {
		return getMax(getMax(number1, number2), number3);
	}

	/**
	 * Get the Maximum of the given 4 number
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	protected int getMax(int number1, int number2, int number3, int number4) {
		return getMax(getMax(number1, number2, number3), number4);
	}

	/**
	 * Get the Minimum of the given two numbers
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	protected int getMin(int number1, int number2) {
		return (number1 <= number2) ? number1 : number2;
	}

	/**
	 * Get the Minimum of the given three numbers
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	protected int getMin(int number1, int number2, int number3) {
		return getMin(getMin(number1, number2), number3);
	}

	/**
	 * Get the Minimum of the given 4 numbers
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	protected int getMin(int number1, int number2, int number3, int number4) {
		return getMin(getMid(number1, number2, number3), number4);
	}

	/**
	 * Calculates the given two numbers sum
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	protected int getSumNumbers(int number1, int number2) {
		return (number1 + number2);
	}

	/**
	 * Return the Sum of the given 3 numbers
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	protected int getSumNumbers(int number1, int number2, int number3) {
		return (number1 + number2 + number3);
	}

	/**
	 * Decides mid number of three numbers
	 */
	protected int getMid(int number1, int number2, int number3) {
		return (getSumNumbers(number1, number2, number3) - getMax(number1, number2, number3)
				- getMin(number1, number2, number3));
	}

	/**
	 * Number is Even
	 */
	protected boolean isEven(int num) {
		return (num % 2 == 0);
	}

	/**
	 * Check and Return "true" if the number is odd
	 * 
	 * @param number
	 * @return
	 */
	protected boolean isOdd(int number) {
		return (number % 2 == 1);
	}

	/**
	 * Return "true", if the numbers are Arithmetic Progression for 3 Numbers
	 */
	protected boolean isArithmeticProgression(int number1, int number2, int number3) {
		return (number2 - number1 == number3 - number2);
	}

	/**
	 * Return "true", if the given numbers are arithmetical progression, otherwise
	 * Return "false" for 4 Numbers
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	protected boolean isArithmeticProgression(int number1, int number2, int number3, int number4) {

		return (isArithmeticProgression(number1, number2, number3)
				&& isArithmeticProgression(number2, number3, number4));
	}

	/**
	 * Return double value two numbers division
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	protected double division(int number1, int number2) {
		return ((double) number1 / number2);
	}

	/**
	 * Decides the numbers are Geometric Progression for 3 Numbers
	 */
	protected boolean isGeometricProgression(int number1, int number2, int number3) {
		return (division(number1, number2) == division(number2, number3));
	}

	/**
	 * Return "true", if the given numbers are Geometric Progression, otherwise
	 * Return "false" for 4 Numbers;
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @param number4
	 * @return
	 */
	protected boolean isGeometricProgression(int number1, int number2, int number3, int number4) {
		return (division(number1, number2) == division(number2, number3)
				&& division(number2, number3) == division(number3, number4));
	}

	/**
	 * The number increases to a given degree
	 * 
	 * @param number
	 * @param index
	 * @return
	 */
	protected int getPow(int number, int degree) {
		int result = 1;
		for (int i = 1; i <= degree; i++) {
			result *= number;
		}

		return result;
	}

	/**
	 * Checks a given number is the degree of the number
	 * 
	 * @param number
	 * @param degree
	 * @return
	 */
	protected boolean isAnyPowerOfBase(int number, int base) {

		if (number == 0) {
			return false;
		}
		while (isMultiple(number, base)) {
			number /= base;
			if (number == 1) {
				return true;
			}
		}

		return false;

	}
	
	/**
	 * Calculate which is degree of the Number...
	 * 
	 * @param number
	 * @param base
	 * @return  isAnyPowerOfBase
	 */
	protected int getPowerOfBase(int number, int base) {
		int count = 0;

		while (isMultiple(number, base)) {
			number /= base;
			count++;
			if (number == 1) {
				return count;
			}
		}

		return NOT_FOUND;
	}

	/**
	 * Check, has the given number Square Root
	 * 
	 * @param number
	 * @return
	 */
	protected boolean hasRoot(int number) {
		for (int i = 1; i <= number / 2; i++) {
			if (getPow(i, 2) == number) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check, if natural numbers by which a given number is divided without a
	 * remainder
	 * 
	 * @param number
	 * @param divisor
	 * @return
	 */
	protected boolean isMultiple(int number, int divisor) {
		return (number % divisor == 0);
	}

	/**
	 * Checks, the given number is not divided by the natural number without
	 * remainder
	 * 
	 * @param number
	 * @param divisor
	 * @return
	 */
	protected boolean isNotMultiple(int number, int divisor) {
		return (!(isMultiple(number, divisor)));
	}

	/**
	 * Check, if natural numbers by which a given number is divided by and remainder
	 * will be "REQUIRID VALUE"
	 * 
	 * @param number
	 * @param divisor
	 * @return
	 */
	protected boolean isMultipleWithRemainder(int number, int divisor, int remainder) {
		return (number % divisor == remainder);
	}

	/**
	 * Checks the given number is Prime
	 * 
	 * @param number
	 * @return
	 */
	protected boolean isPrime(int number) {
		for (int i = 2; i <= number / 2; i++) {
			if (isMultiple(number, i)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Calculates and returns a factorial the given number
	 * 
	 * @param number
	 * @return
	 */
	protected long getFactorial(int number) {
		if (number < 0) {
			return NOT_FOUND;
		}

		int product = 1;

		for (int i = 2; i <= number; i++) {
			product *= i;
		}

		return product;
	}

	/**
	 * Calculates and returns a double factorial the given number
	 * 
	 * @param number
	 * @return
	 */
	protected long getDoubleFactorial(int number) {
		if (number < 0) {
			return NOT_FOUND;
		}

		int product = 1;

		for (int i = number; i > 1; i -= 2) {
			product *= i;
		}

		return product;
	}

	/**
	 * Calculate the current Fibonacci Number with Recursion F[n-1] + F[n-2]
	 * 
	 * @param index
	 * @return
	 */
	protected int getFibonacciNumber(int index) {
		if (index == 1 || index == 2) {
			return 1;
		}
		return getFibonacciNumber(index - 1) + getFibonacciNumber(index - 2);
	}

	/**
	 *Return a new random number in the given range 
	 * 
	 * @param startRange
	 * @param endRange
	 * @return
	 */
	protected int getRandomNumber(int startRange, int endRange) {
		Random random = new Random();
		return startRange + random.nextInt(endRange - startRange + 1);
	}
	
	/**
	 * Check number is in range
	 * 
	 * @param number
	 * @param begin
	 * @param end
	 * @return
	 */
	protected boolean isNumberOfRange(int number, int begin, int end) {
		return (begin <= number && number <= end);
	}
}