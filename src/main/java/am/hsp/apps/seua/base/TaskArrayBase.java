package main.java.am.hsp.apps.seua.base;

/**
 * Base class for all the array. Will contain general methods.
 * 
 * @author lusine
 *
 */
public abstract class TaskArrayBase extends TaskBase {

    /**
     * Create Array and initialize Array elements
     * 
     * @param size
     * @param startRange
     * @param endRange
     * @return
     */
    protected int[] createArray(int size, int startRange, int endRange) {
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = getRandomNumber(startRange, endRange);
        }

        return array;
    }

    /**
     * Create TwoDimensional Array and initialize Array elements
     * 
     * @param size
     * @param startRange
     * @param endRange
     * @return
     */
    protected int[][] createTwoDimensionalArray(int size, int startRange, int endRange) {
        int[][] twoDimensionalArray = new int[size][size];

        for (int i = 0; i < twoDimensionalArray.length; i++) {
            for (int j = 0; j < twoDimensionalArray.length; j++) {
                twoDimensionalArray[i][j] = getRandomNumber(startRange, endRange);
            }
        }
        return twoDimensionalArray;
    }

    /**
     * array[i] = getRandomNumber(startRange, endRange); Print Array
     * 
     * @param array
     */
    protected void printArray(int[] array) {
        System.out.print("Array elemets ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println();
    }

    /**
     * Print Array, where are array[i][j] = getRandomNumber(startRange, endRange);
     * 
     * @param startRange
     * @param endRange
     */
    protected void printTwoDimensionalArray(int[][] array) {

        System.out.println("Array elemets ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(" " + array[i][j] + ", ");
            }
            System.out.println();

        }
    }

    /**
     * Return the count of positive elements
     * 
     * @param array
     * @return
     */
    protected int getCountPositiveElements(int[] array) {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                count++;
            }
        }

        return count;
    }

    /**
     * Return the count of negative elements
     * 
     * @param array
     * @return
     */
    protected int getCountNegativeElements(int[] array) {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                count++;
            }
        }

        return count;
    }

    /**
     * Return the count of zero elements
     * 
     * @param array
     * @return
     */
    protected int getCountZeroElements(int[] array) {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                count++;
            }
        }

        return count;
    }

    /**
     * Return Max element of Array
     * 
     * @param array
     * @return
     */
    protected int getMax(int[] array) {
        int max = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        return max;
    }

    /**
     * Return Min element of Array
     * 
     * @param array
     * @return
     */
    protected int getMin(int[] array) {
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }

        return min;

    }

}
