package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, each
 * third element of which is equal to zero, and the remaining elements are equal
 * to the array value plus maximum value
 * 
 * @author lusine
 *
 */
public class Task_319 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_319 task = new Task_319();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Return a new array, each third element of which is equal to zero, and the
	 * remaining elements are equal to the array value plus maximum value
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int[] requiredArray = new int[array.length];
		int max = getMax(array);

		for (int i = 0; i < array.length; i++) {
			requiredArray[i] = (isMultiple(i + 1, 3)) ? 0 : array[i] + max;
		}

		return requiredArray;

	}

}
