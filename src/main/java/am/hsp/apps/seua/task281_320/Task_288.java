package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given number n and an array[n].... Create and Print a new array, which
 * elements are the odd index elements of the given array
 * 
 * @author lusine
 *
 */
public class Task_288 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_288 task = new Task_288();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, size));
	}

	/**
	 * Return a new array, which elements are the odd index elements of the given
	 * array
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int size) {

		int[] requiredArray = new int[getCountOfOddIndex(size)];
		int j = 0;

		for (int i = 1; i < array.length; i +=2) {
				requiredArray[j++] = array[i];
		}

		return requiredArray;
	}

	/**
	 * Return count of Odd index
	 * @param size
	 * @return
	 */
	private int getCountOfOddIndex(int size) {
		return isEven(size) ? size / 2 : (size - 1) / 2;
	}
}
