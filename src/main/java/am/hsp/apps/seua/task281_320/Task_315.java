package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given number n and an array[n]... Create and Print new array, if there is any
 * element to less than given number, then size of array are equal the count of
 * positive elements and array elements are positive elements the given array,
 * otherwise size of array are equal the count of negative elements and array
 * elements are negative elements the given array
 * 
 * @author lusine
 *
 */
public class Task_315 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_315 task = new Task_315();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, number));

	}

	/**
	 * Decide size of array, initialization array and return... If the condition is
	 * true, then size of array are equal the count of positive elements and array
	 * elements are positive elements the given array, otherwise size of array are
	 * equal the count of negative elements and array elements are negative elements
	 * the given array
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int number) {
		boolean elmLessToNumber = isElmLessFromNumber(array, number);

		int size = (elmLessToNumber) ? getCountPositiveElements(array) : getCountNegativeElements(array);

		int[] requiredArray = new int[size];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (elmLessToNumber) {
				if (array[i] > 0) {
					requiredArray[j++] = array[i];
				}

			} else if (array[i] < 0) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * Check are any element to less than number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private boolean isElmLessFromNumber(int[] array, int number) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] < number) {
				return true;
			}
		}
		return false;
	}

}
