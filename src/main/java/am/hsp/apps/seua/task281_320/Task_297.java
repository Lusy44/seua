package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given number n and an array[n]... Create and Print new array, which elements
 * are absolute value to less, than given number
 * 
 * @author lusine
 *
 */
public class Task_297 extends TaskArrayBase {
	public static void main(String[] args) {
		Task_297 task = new Task_297();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, number));
	}

	/**
	 * Return new array, which elements are absolute value to less, than given
	 * number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int number) {
		int[] requiredArray = new int[(getRequiredCount(array, number))];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isAbsLessThanNumber(array[i], number)) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * Return Count of elements, which absolute value to less, than given number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getRequiredCount(int[] array, int number) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isAbsLessThanNumber(array[i], number)) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Check the absolute value of the element is less than the given number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private boolean isAbsLessThanNumber(int element, int number) {
		return (Math.abs(element) <= number);
	}

}
