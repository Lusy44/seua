package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * The given n number and array[n]... Create and Print new array, which elements
 * index aren't equal array element
 * 
 * @author lusine
 *
 */
public class Task_287 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_287 task = new Task_287();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Create and Return new array, which elements index aren't equal array element
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int[] requiredArray = new int[getCountArrayElmArentEqualIndex(array)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isElmArentEqualIndex(array, i)) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * check Array element is not equal to Index
	 * 
	 * @param array
	 * @param index
	 * @return
	 */
	private boolean isElmArentEqualIndex(int[] array, int index) {
		return (array[index] != index);
	}

	/**
	 * Return count of elements, which array element aren't equal Index
	 * 
	 * @param array
	 * @param index
	 * @return
	 */
	private int getCountArrayElmArentEqualIndex(int[] array) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isElmArentEqualIndex(array, i)) {
				count++;
			}
		}
		return count;
	}
}
