package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, which
 * begin are negative elements, then elements 0-s, then positive elements
 * 
 * @author lusine
 *
 */
public class Task_318 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_318 task = new Task_318();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getSortedArrayByBubbleSort(array));
	}

	/**
	 * Return a new array, which begin are negative elements, then elements 0-s,
	 * then positive elements
	 * 
	 * @param array
	 * @return
	 */
	private int[] getSortedArrayByBubbleSort(int[] array) {
		int[] requiredArray = new int[array.length];
		int k = 0;
		int temp = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;

				}
			}
			requiredArray[k++] = array[i];
		}
		return requiredArray;
	}

}
