package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * The given n number and array[n]... Create and Print new array, which elements
 * that are divided into "DIVISOR" and receive "REMAINDER" balances
 * 
 * @author lusine
 *
 */
public class Task_290 extends TaskArrayBase {
	private static final int DIVISOR = 6;
	private static final int REMAINDER = 1;

	public static void main(String[] args) {
		Task_290 task = new Task_290();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, DIVISOR, REMAINDER));
	}

	/**
	 * Create and Return new array, which elements that are divided into "DIVISOR"
	 * and receive "REMAINDER" balances
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int divisor, int remainder) {
		int[] requiredArray = new int[getCountComplyTheCondition(array, divisor, remainder)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isMultipleWithRemainder(array[i], divisor, remainder)) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * Return the count of Array elements, that are divided into "DIVISOR" and
	 * receive "REMAINDER" balances
	 * 
	 * @param array
	 */
	private int getCountComplyTheCondition(int[] array, int divisor, int remainder) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isMultipleWithRemainder(array[i], divisor, remainder)) {
				count++;
			}
		}
		
		return count;
	}
}