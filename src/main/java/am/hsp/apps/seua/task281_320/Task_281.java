package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * The given n number and array[n]... Create and Print new array, which elements
 * array[n] positive are elements
 * 
 * @author lusine
 *
 */
public class Task_281 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_281 task = new Task_281();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Create and Return new array, whose elements array[n] positive are elements
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int[] requiredArray = new int[getCountPositiveElements(array)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] > 0) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

}
