package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given number n and an array[n]... Create and Print new array, which elements
 * are not multiply "number"
 * 
 * @author lusine
 *
 */
public class Task_292 extends TaskArrayBase {
	private static final int DIVISOR = 7;

	public static void main(String[] args) {
		Task_292 task = new Task_292();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, DIVISOR));

	}

	/**
	 * Create and Return new array, which elements are not multiply "number"
	 * 
	 * @param array
	 * @param divisor
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int divisor) {
		int[] requiredArray = new int[getRequiredCount(array, divisor)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isNotMultiple(array[i], divisor))
				requiredArray[j++] = array[i];
		}

		return requiredArray;
	}

	/**
	 * Return count a new array, which elements are not multiply "number"
	 * 
	 * @param array
	 * @param divisor
	 * @return
	 */
	private int getRequiredCount(int[] array, int divisor) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isNotMultiple(array[i], divisor)) {
				count++;
			}
			
		}

		return count;
	}

}
