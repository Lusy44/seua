package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * The given n number and array[n]... Create and Print new array, which elements
 * from A to B: [a:b]
 * 
 * @author lusine
 *
 */
public class Task_284 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_284 task = new Task_284();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int beginRange = getUserInput("Please enter begin A of range");
		int finishRange = getUserInput("Please enter finish B of range");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array, beginRange, finishRange));

	}

	/**
	 * Return a new array, which elements from A to B: [a:b]
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array, int beginRange, int finishRange) {
		int[] requiredArray = new int[getCountElmOfRange(array, beginRange, finishRange)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isNumberOfRange(array[i], beginRange, finishRange)) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	

	/**
	 * Return count of elements, which from begin to end
	 * 
	 * @param array
	 * @param begin
	 * @param end
	 * @return
	 */
	private int getCountElmOfRange(int[] array, int begin, int end) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			//if (isNumberOfRange(array[i], begin, end)) {
			if(isNumberOfRange(array[i], begin, end))
				count++;
			}
		

		return count;
	}

}
