package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, which
 * elements are absolute values less than the arithmetic average of Min and Max
 * elements
 * 
 * @author lusine
 *
 */
public class Task_316 extends TaskArrayBase {
	public static void main(String[] args) {
		Task_316 task = new Task_316();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Return a new array, which elements are absolute values less than the
	 * arithmetic average of Min and Max elements
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		double arithmeticAvg = getArithmeticAvgOfMinAndMax(array);
		int[] requiredArray = new int[getRequiredCount(array, arithmeticAvg)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (isAbsValueElmLessThanArithmeticAvg(array[i], arithmeticAvg)) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * Return arithmetic average of min and max elements
	 * 
	 * @param array
	 * @return
	 */
	private double getArithmeticAvgOfMinAndMax(int[] array) {
		int min = array[0];
		int max = array[0];

		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
			if (array[i] < min) {
				min = array[i];
			}
		}

		return (double) (max + min) / 2;
	}

	/**
	 * Return count are elements, which absolute values less than the arithmetic
	 * average
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getRequiredCount(int[] array, double arithmeticAvg) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isAbsValueElmLessThanArithmeticAvg(array[i], arithmeticAvg)) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Check is element to less than number arithmetic average
	 * 
	 * @param element
	 * @param arithmeticAvg
	 * @return
	 */
	private boolean isAbsValueElmLessThanArithmeticAvg(int element, double arithmeticAvg) {
		return (Math.abs(element) < arithmeticAvg);
	}

}
