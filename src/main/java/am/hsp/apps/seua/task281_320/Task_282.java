package main.java.am.hsp.apps.seua.task281_320;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given number n and an array[n]... Create and Print a new array, which
 * elements are the positive and negative elements of the given array.
 * 
 * @author lusine
 *
 */
public class Task_282 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_282 task = new Task_282();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start array of range");
		int endRange = getUserInput("Please enter end array of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Return a new array, which elements are the positive and negative elements the
	 * given array
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int size = array.length - getCountZeroElements(array);
		int[] requiredArray = new int[size];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				requiredArray[j++] = array[i];
			}
		}

		return requiredArray;
	}

}
