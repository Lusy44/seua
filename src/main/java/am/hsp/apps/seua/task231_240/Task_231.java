package main.java.am.hsp.apps.seua.task231_240;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum square of even value Array elements
 * 
 * @author lusine
 *
 */
public class Task_231 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_231 task = new Task_231();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result: " + getSumSquareComplyTheCondition(array));
	}

	/**
	 * Return the sum square of even value Array elements
	 * 
	 * @param array
	 */
	private int getSumSquareComplyTheCondition(int[] array) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (isEven(array[i])) {
				sum += getPow(array[i], 2);
			}
		}
		return sum;
	}
}
