package main.java.am.hsp.apps.seua.task231_240;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum and the product of even value Array elements
 * 
 * @author lusine
 *
 */
public class Task_233 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_233 task = new Task_233();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printSumAndProductOfEvenValue(array);
	}

	/**
	 * Print the sum and the product of even value Array elements
	 * 
	 * @param array
	 */
	private void printSumAndProductOfEvenValue(int[] array) {
		int sum = 0;
		int product = 1;

		for (int i = 0; i < array.length; i++) {
			if (isEven(array[i])) {
				sum += array[i];
				product *= array[i];
			}
		}
		System.out.println("The sum: " + sum);
		System.out.println("The product: " + product);
	}
}
