package main.java.am.hsp.apps.seua.task231_240;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the arithmetic average of  Array elements, which divide by number without remainder
 * 
 * @author lusine
 *
 */
public class Task_238 extends TaskArrayBase {
	private static final int DIVISOR = 3;

	public static void main(String[] args) {
		Task_238 task = new Task_238();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Arithmetic average = " + getArithmeticAverageComplyTheCondition(array, DIVISOR));
	}

	/**
	 * Return the arithmetic average of  Array elements, which divide by number without remainder
	 * 
	 * @param array
	 */
	private double getArithmeticAverageComplyTheCondition(int[] array, int divisor) {
		int sum = 0;
		int count = 0;
		
		for (int i = 0; i < array.length; i++) {
			if (isMultiple(array[i], divisor)) {
				sum += array[i];
				count++;
			}
		}
		return (double)sum / count;
	}
}
