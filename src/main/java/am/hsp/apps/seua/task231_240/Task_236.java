package main.java.am.hsp.apps.seua.task231_240;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the count and the product of odd value Array elements
 * 
 * @author lusine
 *
 */
public class Task_236 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_236 task = new Task_236();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printCountAndProductOfOddValue(array);
	}

	/**
	 * Print the count and the product of odd value Array elements
	 * 
	 * @param array
	 */
	private void printCountAndProductOfOddValue(int[] array) {
		int count = 0;
		int product = 1;

		for (int i = 0; i < array.length; i++) {
			if (isOdd(array[i])) {
				count++;
				product *= array[i];
			}
		}
		System.out.println("The count: " + count);
		System.out.println("The product: " + product);
	}
}
