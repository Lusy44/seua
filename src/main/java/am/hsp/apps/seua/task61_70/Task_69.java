package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "Return value1" , if the sum of Digits greater than "Required value",
 * otherwise print "Return value2"
 * 
 * @author lusine
 *
 */
public class Task_69 extends TaskDigitBase {
	private static final int RETURN_VALUE1 = 1;
	private static final int RETURN_VALUE2 = 0;

	private static final int REQUIRED_VALUE = 20;

	public static void main(String[] args) {
		Task_69 task = new Task_69();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println("Result: y =  " + getResult(number));

	}

	/**
	 * Return "Return value1", if the sum of Digits greater than "Required value",
	 * otherwise Return "Return value2"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {

		if (getSumOfDigit(number) > REQUIRED_VALUE) {
			return RETURN_VALUE1;
		}
		return RETURN_VALUE2;
	}
}
