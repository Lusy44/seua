package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Print Ones multiplied Hundreds Digit, if Ones greater than
 * Tens, otherwise Print "Required value"
 * 
 * @author lusine
 *
 */
public class Task_68 extends TaskDigitBase {
	private static final int REQ_VALUE = 1;

	public static void main(String[] args) {
		Task_68 task = new Task_68();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println("Result: y =  " + getResult(number));

	}

	/**
	 * Return Ones multiplied Hundreds Digit, if ones greater than Tens, otherwise
	 * Return "Required value"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		int ones = getOnes(number);
		if ( ones > getTens(number)) {
			return (ones * getHundreds(number));
		}
		return REQ_VALUE;
	}
}
