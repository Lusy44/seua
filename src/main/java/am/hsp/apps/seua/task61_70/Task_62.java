package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Print the number divided by the sum of Ones and Hundreds, if
 * the number is less than Required value, otherwise Print the number divided by the
 * sum of Tens and Thousands
 * 
 * @author lusine
 *
 */
public class Task_62 extends TaskDigitBase {
	private static final int REQ_VALUE = 5000;

	public static void main(String[] args) {
		Task_62 task = new Task_62();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println(getResult(number));

	}

	/**
	 * Calculate and Return the number divided by the sum of Ones and Hundreds, if
	 * the number is less than Required value, otherwise Return the number divided by the
	 * sum of Tens and Thousands
	 * 
	 * @param number
	 * @return
	 */
	private double getResult(int number) {
		if (number < REQ_VALUE) {
			return (division(number, getOnes(number) + getHundreds(number)));
		}
		return (division(number, getTens(number) + getThousands(number)));
	}

}
