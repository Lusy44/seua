package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "Required value 1" , if the Ones multiplied Tens Digit equal "Required
 * value 1", otherwise Print "Required value 2"
 * 
 * @author lusine
 *
 */
public class Task_65 extends TaskDigitBase {
	private static final int REQ_VALUE1 = 12;
	private static final int REQ_VALUE2 = 0;

	public static void main(String[] args) {
		Task_65 task = new Task_65();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println("Result: y =  " + getResult(number));

	}

	/**
	 * Return "Required value 1" , if the Ones multiplied Tens Digit equal "Required
	 * value 1", otherwise Return "Required value 2"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		if (getOnes(number) * getTens(number) == REQ_VALUE1) {
			return REQ_VALUE1;
		}
		return REQ_VALUE2;
	}
}
