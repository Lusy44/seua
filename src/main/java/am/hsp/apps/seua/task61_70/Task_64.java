package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "ANSWER_A", if the sum of Ones and Tens Digit equal "Required Value",
 * otherwise Print "ANSWER_B"
 * 
 * @author lusine
 *
 */
public class Task_64 extends TaskDigitBase {
	private static final int REQ_VALUE = 5;
	private static final char ANSWER_A = 'S';
	private static final char ANSWER_B = 'D';

	public static void main(String[] args) {
		Task_64 task = new Task_64();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println("Result: y =  " + getResult(number));

	}

	/**
	 * Return "ANSWER_A", if the sum of Ones and Tens Digit equal "Required Value",
	 * otherwise Return "ANSWER_B"
	 * 
	 * @param number
	 * @return
	 */
	private char getResult(int number) {
		if (getOnes(number) + getTens(number) == REQ_VALUE) {
			return ANSWER_A;
		}
		return ANSWER_B;
	}
}
