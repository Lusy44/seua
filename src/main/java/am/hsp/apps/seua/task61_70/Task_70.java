package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/*
 *  Print "Return value1" , if the multiplication of Digits greater than "Required value", 
 *  otherwise print "Return value"
 */
public class Task_70 extends TaskDigitBase {
	public static final int RETURN_VALUE1 = 0;
	public static final int RETURN_VALUE2 = 1;

	public static final int REQUIRED_VALUE = 200;

	public static void main(String[] args) {
		Task_70 task = new Task_70();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("PLease enter four digit number");
		System.out.println("Result: y =  " + getResult(number));

	}

	/**
	 * Return "Return value1" , if the multiplication of Digits greater than "Required
	 * value", otherwise Return "Return value2"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {

		if (getProductOfDigits(number) > REQUIRED_VALUE) {
			return RETURN_VALUE1;
		}
		return RETURN_VALUE2;
	}

}
