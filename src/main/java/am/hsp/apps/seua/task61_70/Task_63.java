package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "REQ_VALUE1", if the Any digits equal "Required value1", otherwise
 * Print "REQ_VALUE2"
 * 
 * @author lusine
 *
 */
public class Task_63 extends TaskDigitBase {
	private static final int REQ_VALUE1 = 1;
	private static final int REQ_VALUE2 = 0;

	public static void main(String[] args) {
		Task_63 task = new Task_63();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println(getResult(number));

	}

	/**
	 * Return "true", if Any the number equal Required value1, otherwise return
	 * "false"
	 * 
	 * @param number
	 * @return
	 */
	private int getResult(int number) {
		if (getOnes(number) == REQ_VALUE1 || getTens(number) == REQ_VALUE1 || getHundreds(number) == REQ_VALUE1
				|| getThousands(number) == REQ_VALUE1) {
			return REQ_VALUE1;
		}
		return REQ_VALUE2;
	}
}
