package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "ANSWER_1" , if the Ones or Thousands Digit equal "Required value",
 * otherwise Print "ANSWER_2"
 * 
 * @author lusine
 *
 */
public class Task_66 extends TaskDigitBase {
	private static final int REQ_VALUE = 4;
	private static final String ANSWER_1 = "Yes";
	private static final String ANSWER_2 = "No";

	public static void main(String[] args) {
		Task_66 task = new Task_66();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		getResult(number);

	}

	/**
	 * Return "ANSWER_1;" , if the Ones or Thousands Digit equal "Required value",
	 * otherwise Return "ANSWER_2;"
	 * 
	 * @param number
	 * @return
	 * @return
	 */
	private String getResult(int number) {
		if (getOnes(number) == REQ_VALUE || (getThousands(number) == REQ_VALUE)) {
			return ANSWER_1;
		}
		return ANSWER_2;

	}

}
