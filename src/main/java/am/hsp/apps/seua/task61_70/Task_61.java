package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/*
 * Print "true" if the of Sum Ones and Tens Digits equal the sum of Hundreds and Thousands, 
 * otherwise print "false"
 */
public class Task_61 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_61 task = new Task_61();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		System.out.println(getResult(number));

	}

	/*
	 * Return "true", if the of Sum Ones and Tens Digits equal the sum of Hundreds
	 * and Thousands, otherwise Return "false"
	 */
	private boolean getResult(int number) {
		return (getOnes(number) + getTens(number) == getHundreds(number) + getThousands(number));

	}

}
