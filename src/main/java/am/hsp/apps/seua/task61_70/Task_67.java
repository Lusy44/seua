package main.java.am.hsp.apps.seua.task61_70;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "ANSWER_A" , if the four digit number equal square of the sum of
 * digits, otherwise Print "ANSWER_B"
 * 
 * @author lusine
 *
 */
public class Task_67 extends TaskDigitBase {
	private static final String ANSWER_A = "Yes";
	private static final String ANSWER_B = "No";

	public static void main(String[] args) {
		Task_67 task = new Task_67();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please Enter  four digit number");
		getResult(number);

	}

	/**
	 * Return "ANSWER_A", if the four digit number equal square of the sum of digits,
	 * otherwise Return "ANSWER_B"
	 * 
	 * @param number
	 * @return
	 */
	private String getResult(int number) {
		int sum = getSumOfDigit(number);
		if (number == getPow(sum, 2)) {
			return ANSWER_A;
		}
		return ANSWER_B;
	}

}
