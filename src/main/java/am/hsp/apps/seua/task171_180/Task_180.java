package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if a given number is the power of the base, otherwise Print
 * "false"
 * 
 * @author lusine
 *
 */
public class Task_180 extends TaskBase {
	private static int BASE = 3;

	public static void main(String[] args) {
		Task_180 task = new Task_180();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Plase, enter number");
		System.out.println(isAnyPowerOfBase(number, BASE));

	}

}
