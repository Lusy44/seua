package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;;

/**
 * Calculates and returns a double factorial the given number
 * 
 * @author lusine
 *
 */
public class Task_172 extends TaskBase {
	private static final int NOT_FOUND = -1;
	public static void main(String[] args) {
		Task_172 task = new Task_172();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number");
		System.out.println("Result getDoubleFactorial= " + getDoubleFactorial(number));
		System.out.println("Result getDoubleFactorialWithRecursion= " + getDoubleFactorialWithRecursion(number));
	}
   /**
    * Calculates and returns a double factorial with method Recursion the given number
    * @param number
    * @return
    */
	private int getDoubleFactorialWithRecursion(int number) {
		if (number < 0) {
			return NOT_FOUND;
		}
		if (number == 1 || number == 0) {
			return 1;
		}
		
		if (number == 2) {
			return 2;
		}

		return getDoubleFactorialWithRecursion(number - 2) * number;
	}

}
