package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Count and Print Fibonacci Series: F[n] = F[n-1] + F[n-2], where is F[1] = 1,
 * F[2] = 1; Print and Recursion and Normal method
 * 
 * @author lusine
 *
 */
public class Task_176 extends TaskBase {
	private static final int FIRST_NUMBER = 1;
	private static final int SEC_NUMBER = 1;

	public static void main(String[] args) {
		Task_176 task = new Task_176();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int number = getUserInput("Please enter number");

		printFibonacci(number);
		printFibonacciWithRecursion(number);

	}

	/**
	 * Print Fibonacci Series: F[n] = F[n-1] + F[n-2], where is F[1] = 1
	 * 
	 * @param number
	 */
	protected void printFibonacci(int number) {
		System.out.print("Fibonacci " + FIRST_NUMBER + ", " + SEC_NUMBER + ", ");
		int previous = SEC_NUMBER;
		int prePrevious = FIRST_NUMBER;

		for (int i = 3; i <= number; i++) {
			int current = getCurrentFibonacciNumber(previous, prePrevious);
			System.out.print(current + ", ");

			previous = prePrevious;
			prePrevious = current;
		}
		System.out.println();
	}

	/**
	 * Calculate the current Fibonacci Number: F[n-1] + F[n-2]
	 * 
	 * @param previousElement
	 * @param prePreviousElement
	 * @return
	 */
	private int getCurrentFibonacciNumber(int previousElement, int prePreviousElement) {
		return previousElement + prePreviousElement;
	}

	/**
	 * System.out.println F[n] = F[n-1] + F[n-2] with Recursion
	 * 
	 * @param number
	 */
	private void printFibonacciWithRecursion(int number) {
		System.out.print("Fibonacci with Recursion " + FIRST_NUMBER + ", " + SEC_NUMBER + ", ");
		for (int i = 3; i <= number; i++) {
			System.out.print(getFibonacciNumber(i) + ", ");
		}

	}

}
