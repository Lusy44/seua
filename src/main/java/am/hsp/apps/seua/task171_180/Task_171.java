package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Calculates and returns a factorial the given number
 * 
 * @author lusine
 *
 */
public class Task_171 extends TaskBase {
	private static final int NOT_FOUND = -1;

	public static void main(String[] args) {
		Task_171 task = new Task_171();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number");
		System.out.println("Result getFactoial = " + getFactorial(number));
		System.out.println("Result getFactoialWithRecursion = " + getFactoialWithRecursion(number));

	}

	/**
	 * Calculates and returns a factorial with method Recursion the given number
	 * 
	 * @param number
	 * @return
	 */
	private int getFactoialWithRecursion(int number) {
		if (number < 0) {
			return NOT_FOUND;
		}
		
		if (number == 0 || number == 1) {
			return 1;
		}
		
		return number * getFactoialWithRecursion(number - 1);

	}

}
