package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Count and Print the following sequence x[n] = 2 + 1 / x[n-1], where is x[0]
 * equal FIRST_NUMBER. Print and Recursion and Normal method
 * 
 * @author lusine
 *
 */
public class Task_174 extends TaskBase {
	private static final int FIRST_NUMBER = 2;

	public static void main(String[] args) {
		Task_174 task = new Task_174();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please, enter number");

		printSequence(number);
		printSequenceWithRecursion(number);

	}

	/**
	 * System.out.println the following sequence: x[n] = 2 + 1 / x[n-1];
	 * 
	 * @param number
	 */
	private void printSequence(int number) {
		double previous = FIRST_NUMBER;

		System.out.println("Result" + "\n" + FIRST_NUMBER);

		for (int i = 1; i < number; i++) {
		double current = getCurrentElement(previous);
			System.out.println(current);
			previous = current;
		}
		System.out.println();
	}

	/**
	 * Calculates the current element with normal method: 2 + 1 / x[n-1]
	 * 
	 * @param previous
	 * @return
	 */
	private double getCurrentElement(double previous) {
		return (2 + 1 / previous);
	}

	/**
	 * System.out.println the following sequence with Recursion: x[n] = 2 + 1 /
	 * x[n-1]
	 * 
	 * @param index
	 * @return
	 */
	private void printSequenceWithRecursion(int number) {
		System.out.println("Result with Recursion");
		for (int i = 0; i < number; i++) {
			double current = getCurrentElementWithRecursion(i);

			System.out.println(current);
		}
	}

	/**
	 * Calculates the current element with normal Recursion: 2 + 1 / x[n-1]
	 * 
	 * @param index
	 * @return
	 */
	private double getCurrentElementWithRecursion(int index) {
		if (index == 0) {
			return FIRST_NUMBER;
		}
		return (2 + 1 / getCurrentElementWithRecursion(index - 1));
	}

}
