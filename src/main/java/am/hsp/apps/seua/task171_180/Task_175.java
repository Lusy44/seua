package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Count and Print the following sequence x[n] = (x[n-1] + 1) / n, where is x[0]
 * equal FIRST_NUMBER. Print and Recursion and Normal method
 * 
 * @author lusine
 *
 */
public class Task_175 extends TaskBase {
	private static final int FIRST_NUMBER = 1;

	public static void main(String[] args) {
		Task_175 task = new Task_175();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		
		printSequence(number);
		printSequenceWithRecursion(number);

	}

	/**
	 * Print the following sequence: x[n] = (x[n-1] + 1) / n
	 * 
	 * @param number
	 */
	private void printSequence(int number) {
		double previous = FIRST_NUMBER;
		System.out.println("The Sequence " + "\n"+ FIRST_NUMBER);
		
		for (int i = 1; i < number; i++) {
			double current = getCurrentElement(previous, i);
			System.out.println(current);
			previous = current;
		}
		System.out.println();
	}
	
	/**
	 * Calculates the current element with normal method: (x[n-1] + 1) / n
	 * 
	 * @param previous
	 * @return
	 */
	private double getCurrentElement(double previous, int index) {
		return (previous + 1) / index;
	}
	

	/**
	 * System.out.println x[n] = (x[n-1] + 1) / n, with Recursion
	 * 
	 * @param number
	 */
	private void printSequenceWithRecursion(int number) {
		System.out.println("The Sequence with Recursion");
		
		for (int i = 0; i < number; i++) {
			double current = getCurrentElementWithRecursion(i);
			System.out.println(current);
		}
	}

	/**
	 * Calculate current element with Recursion: (x[n-1] + 1) / n
	 * 
	 * @param index
	 * @return
	 */
	private double getCurrentElementWithRecursion(int index) {
		if (index == 0) {
			return 1;
		}
		return (getCurrentElementWithRecursion(index - 1) + 1) / index;
	}



}
