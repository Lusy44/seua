package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Count and Print the following sequence x[n] = x[n-1] + x[n-2] - 2*x[n-3],
 * where are x[1] = 1, x[2] = 2, x[3] = 3. Print and Recursion and Normal method
 * 
 * @author lusine
 *
 */
public class Task_178 extends TaskBase {
	private static final int FIRST_NUMBER = 1;
	private static final int SEC_NUMBER = 2;
	private static final int THREE_NUMBER = 3;

	public static void main(String[] args) {
		Task_178 task = new Task_178();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		printSequence(number);
		printSequenceWithRecursion(number);

	}

	/**
	 * Print the following sequence x[n] = x[n-1] + x[n-2] - 2*x[n-3]
	 * 
	 * @param number
	 */
	private void printSequence(int number) {
		
		int previous = FIRST_NUMBER;
		int prePrevious = SEC_NUMBER;
		int prePrePrevious = THREE_NUMBER;
		System.out.print("The Sequence" + ", " + FIRST_NUMBER + ", " + SEC_NUMBER + ", " + THREE_NUMBER + ", ");
		
		for (int i = 4; i <= number; i++) {
			int current = getCurrentElement(previous, prePrevious, prePrePrevious);
			System.out.print(current + ", ");
			
			previous = prePrevious;
			prePrevious = prePrePrevious;
			prePrePrevious = current;
		}
		System.out.println();
	}
	
	/**
	 * Calculates the current element with normal method x[n-1] + x[n-2] - 2*x[n-3]
	 * 
	 * @param previous
	 * @return
	 */
	private int getCurrentElement(int previousElement, int prePreviousElement, int prePrePreviousElement) {
		return prePrePreviousElement + prePreviousElement - 2 * previousElement; 
	}

	/**
	 * Print x[n] = x[n-1] + x[n-2] - 2*x[n-3], with Recursion
	 * 
	 * @param number
	 */
	private void printSequenceWithRecursion(int number) {
		System.out.print("Result with Recursion " + FIRST_NUMBER + ", " + SEC_NUMBER + ", " + THREE_NUMBER + ", ");
		for (int i = 4; i <= number; i++) {
			System.out.print(getCurrentElementWithRecursion(i) + ", ");
		}
	}
	
	/**
	 *Calculate current element with Recursion x[n-1] + x[n-2] - 2*x[n-3]
	 * with Recursion
	 * 
	 * @param index
	 * @return
	 */
	private int getCurrentElementWithRecursion(int index) {
		if (index == 1) {
			return 1;
		}
		if (index == 2) {
			return 2;
		}
		if (index == 3) {
			return 3;
		}

		return getCurrentElementWithRecursion(index - 1) + getCurrentElementWithRecursion(index - 2)
				- 2 * getCurrentElementWithRecursion(index - 3);

	}


}
