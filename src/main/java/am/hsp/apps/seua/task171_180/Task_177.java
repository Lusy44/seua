package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Count and Print the following sequence x[n] = (x[n-2] + 2*x[n-1]) / 3, where
 * are x[1] = 1, x[2] = 2 ; Print and Recursion and Normal method
 * 
 * @author lusine
 *
 */
public class Task_177 extends TaskBase {
	private static final int FIRST_NUMBER = 1;
	private static final int SEC_NUMBER = 2;

	public static void main(String[] args) {
		Task_177 task = new Task_177();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		printSequence(number);
		printSequenceWithRecursion(number);
		
	}

	/**
	 * Print the following sequence: x[n] = (x[n-2] + 2*x[n-1]) / 3
	 * 
	 * @param number
	 */
	private void printSequence(int number) {
		System.out.println("The Sequence " + "\n" + FIRST_NUMBER + "\n" + SEC_NUMBER);
		double previous = SEC_NUMBER;
		double prePrevious = FIRST_NUMBER;

		for (int i = 3; i <= number; i++) {
			double current = getCurrentElement(previous, prePrevious);
			System.out.println(current);

			prePrevious = previous;
			previous = current;
		}
	}

	/**
	 * Calculates the current element with normal method (x[n-2] + 2*x[n-1]) / 3
	 * 
	 * @param previuosElement
	 * @param prePreviuosElement
	 * @return
	 */
	private double getCurrentElement(double previuosElement, double prePreviuosElement) {
		return (prePreviuosElement + 2 * previuosElement) / 3;
	}

	/**
	 * Print the Sequence with Recursion
	 * @param number
	 */
	private void printSequenceWithRecursion(int number) {
		System.out.println("The Sequence with Recursion" + "\n" + FIRST_NUMBER + "\n" + SEC_NUMBER);
		for (int i = 3; i <= number; i++) {
			System.out.println(getCurrentElementWithRecursion(i));
		}
	}

	/**
	 * Calculate current element with Recursion (x[n-2] + 2*x[n-1]) / 3
	 * 
	 * @param index
	 * @return
	 */
	private double getCurrentElementWithRecursion(int index) {
		if (index == 1) {
			return 1;
		}
		if (index == 2) {
			return 2;
		}
		return (getCurrentElementWithRecursion(index - 2) + 2 * getCurrentElementWithRecursion(index - 1)) / 3;
	}

}
