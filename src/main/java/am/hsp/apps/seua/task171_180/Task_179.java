package main.java.am.hsp.apps.seua.task171_180;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Use only "minus" and "plus", print the firstNumber divison secondNumber the received
 * number the Quotient and the Balance
 * 
 * @author lusine
 *
 */
public class Task_179 extends TaskBase {

	public static void main(String[] args) {
		Task_179 task = new Task_179();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int firstNumber = getUserInput("Plaese enter firstNumber");
		int secondNumber = getUserInput("Please, enter secondNumber");

		getQuotient(firstNumber, secondNumber);

	}

	/**
	 * Use only "minus" and "plus", print the firstNumber divison secondNumber the received
	 * number the Quotient and the Balance
	 * 
	 * @param firstNumber
	 * @param secondNumber
	 */
	private void getQuotient(int firstNumber, int secondNumber) {
		int quotient = 0;

		while(firstNumber >= secondNumber) {
			firstNumber -= secondNumber;
			quotient++;
		}
		
	    System.out.println("The Quotient = " + quotient + "\n" + "The Balance = " + firstNumber);
		
	}

}
