package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print that number, which it turns out from the given number digit write right
 * to left
 * 
 * @author lusine
 *
 */
public class Task_206 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_206 task = new Task_206();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		
		System.out.println(getNumberRightToLeft(number));
	}

	/**
	 * Return Number from Right to left
	 * @param number
	 * @return
	 */
	private int getNumberRightToLeft (int number) {
		int tempNumber = 0;
		
		while(number > 0) {
			tempNumber = addDigitToNumber(tempNumber, getOnes(number));
			number = removeLastDigit(number);
		}
		
		return tempNumber;
	} 
	
	/**
	 * Add digit to number
	 * @param number
	 * @param digit
	 * @return
	 */
	private int addDigitToNumber(int number, int digit) {
		return 10 * number + digit;
	}
	
}
