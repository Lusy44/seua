package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print the given number from right to left
 * 
 * @author lusine
 *
 */
public class Task_204 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_204 task = new Task_204();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");
		printNumberRightToLeft(number);

	}

	/**
	 * Print the given number from right to left
	 * 
	 * @param number
	 */
	private void printNumberRightToLeft(int number) {
		while (number > 0 ) {
			System.out.print(getOnes(number) + ", ");
			
			number = removeLastDigit(number);

		}
	}

}
