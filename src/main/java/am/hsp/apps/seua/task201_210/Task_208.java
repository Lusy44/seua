package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print out "true" if from a given number all digits equals, otherwise print
 * "false"
 * 
 * @author lusine
 *
 */
public class Task_208 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_208 task = new Task_208();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println(areAllDigitsEqual(number));
	}

	/**
	 * Return "true" if from a given number all digits equals, otherwise Return
	 * "false"
	 * 
	 * @param number
	 * @return
	 */
	private boolean areAllDigitsEqual(int number) {
		int ones = getOnes(number);
		number = removeLastDigit(number);
		
		while (number > 0) {
			if (ones != getOnes(number)) {
				return false;
			}
			number = removeLastDigit(number);
		}

		return true;
	}
}
