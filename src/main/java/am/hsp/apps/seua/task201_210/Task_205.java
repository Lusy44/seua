package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print the given number from left to right
 * 
 * @author lusine
 *
 */
public class Task_205 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_205 task = new Task_205();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("PLease enter number");

		printNumberFromLeftToRight(number);
	}

	/**
	 * Print the given number from left to right
	 * 
	 * @param number
	 */
	private void printNumberFromLeftToRight(int number) {
		int count = getDigitCount(number);

		while(count > 0) {
			System.out.print(getDigitByIndex(number, count--) + ", ");
		}
	}

}
