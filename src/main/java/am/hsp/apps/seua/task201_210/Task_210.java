package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate sum of odd digits and calculate sum of even digits. Print "true",
 * if the two sums are equal, otherwise print "false"
 * 
 * @author lusine
 *
 */
public class Task_210 extends TaskDigitBase {
	public static void main(String[] args) {
		Task_210 task = new Task_210();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println("Result = " + isEqualNumbers(number));
	}

	/**
	 * Check is the numbers equal
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	private boolean isEqualNumbers(int number) {
		return (getSumEvenDigit(number) == getSumOddDigit(number));
	}
	


}
