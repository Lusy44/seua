package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "true" if from the given number digits there is odd digit, otherwise
 * Print "false"
 * 
 * @author lusine
 *
 */
public class Task_209 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_209 task = new Task_209();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println("Result = " + isOddDigit(number));

	}

	/**
	 * Return "true" if from the given number digits there is odd digit, otherwise
	 * Return "false"
	 * 
	 * @param number
	 * @return
	 */
	private boolean isOddDigit(int number) {
		while (number > 0) {
			if (isOdd(getOnes(number))) {
				return true;
			}
			number = removeLastDigit(number);
		}
		
		return false;
	}

}
