package main.java.am.hsp.apps.seua.task201_210;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print out "true" if from a given number any digit is NUMBER, otherwise print
 * "false"
 * 
 * @author lusine
 *
 */
public class Task_207 extends TaskDigitBase {
	private static final int NUMBER = 2;

	public static void main(String[] args) {
		Task_207 task = new Task_207();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter number");

		System.out.println("Result = " + isAnyDigitEqualNumber(number));

	}

	/**
	 * Check from a given number any digit is NUMBER
	 * 
	 * @param number
	 * @return
	 */
	private boolean isAnyDigitEqualNumber(int number) {
		while (number > 0) {
			if (getOnes(number) == NUMBER) {
				return true;
			}
			number = removeLastDigit(number);
		}
		return false;
	}
}
