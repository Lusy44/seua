package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum, which equal sum of the elements after the first max. If the
 * max is last element, then Print NUMBER
 * 
 * @author lusine
 *
 */
public class Task_345 extends TaskArrayBase {
	private static final int NUMBER = 0;

	public static void main(String[] args) {
		Task_345 task = new Task_345();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println(getRequiredSum(array));
	}

	/**
	 * Return the sum, which equal sum of the elements after the first max. If the
	 * max is last element, then return NUMBER
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredSum(int[] array) {
		int index = getFirstMaxIndex(array);

		if (index == array.length - 1) {
			return NUMBER;
		}

		return getSumAfterFirstMax(array, index);
	}

	/**
	 * Return first index Max element of array
	 * 
	 * @param array
	 * @return
	 */
	private int getFirstMaxIndex(int[] array) {
		int max = array[0];
		int index = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
				index = i;
			}
		}

		return index;
	}

	/**
	 * Return sum of the elements after the first max
	 * 
	 * @param array
	 * @param max
	 * @return
	 */
	private int getSumAfterFirstMax(int array[], int indexOfFirstMax) {
		int sum = 0;

		for (int i = indexOfFirstMax + 1; i < array.length; i++) {
			sum += array[i];
		}

		return sum;
	}

}
