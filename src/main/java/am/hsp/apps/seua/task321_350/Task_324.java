package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, which
 * elements are at first to greater to array[0], then equal array[0], then to
 * less from array[0].
 * 
 * @author lusine
 *
 */
public class Task_324 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_324 task = new Task_324();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequiredArray(array));

	}

	/**
	 * Return a new array, which elements are at first to greater to array[0], then
	 * equal array[0], then to less from array[0].
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int indexByGreaterElm = 0;
		int indexByEqualElm = getCountElmGreaterNumber(array, array[0]);
		int indexByLessElm = array.length - 1;

		int[] requiredArray = new int[array.length];

		for (int i = 0; i < array.length; i++) {
			if (array[i] > array[0]) {
				requiredArray[indexByGreaterElm++] = array[i];
			}

			if (array[i] == array[0]) {
				requiredArray[indexByEqualElm++] = array[i];
			}

			if (array[i] < array[0]) {
				requiredArray[indexByLessElm--] = array[i];
			}
		}

		return requiredArray;
	}

	/**
	 * Return count of elements, which are greater, than array[0];
	 * 
	 * @param array
	 * @return
	 */
	private int getCountElmGreaterNumber(int[] array, int number) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] > number) {
				count++;
			}
		}

		return count;
	}

}
