package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, which
 * elements are the indexes of the minimum elements.
 * 
 * @author lusine
 *
 */
public class Task_327 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_327 task = new Task_327();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray(getRequredArray(array));
	}

	/**
	 * Return and print a new array, which elements are the indexes of the minimum
	 * elements
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequredArray(int[] array) {
		int min = getMin(array);
		int[] requiredArray = new int[getCountMin(array)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] == min) {
				requiredArray[j++] = i;
			}
		}

		return requiredArray;
	}

	/**
	 * Return count of minimum
	 * 
	 * @param array
	 * @return
	 */
	private int getCountMin(int[] array) {
		int count = 0;
		int min = getMin(array);

		for (int i = 0; i < array.length; i++) {
			if (array[i] == min) {
				count++;
			}
		}

		return count++;
	}

}
