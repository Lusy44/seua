package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum of elements, which at least have once a equal element
 * 
 * @author lusine
 *
 */
public class Task_342 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_342 task = new Task_342();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println(getRequiredSum(array));
	}

	/**
	 * Return the sum of elements, which at least have once a equal element
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredSum(int[] array) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (doesContain(array, i)) {
				sum += array[i];
			}
		}

		return sum;
	}

	/**
	 * Check there is the element of an equal number of arrays
	 * 
	 * @param array
	 * @param index
	 * @return
	 */
	private boolean doesContain(int[] array, int index) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == array[index] && i != index) {
				return true;
			}
		}

		return false;
	}

}
