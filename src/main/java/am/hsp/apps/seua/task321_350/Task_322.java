package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array, add a
 * two zero value after the 0-s element
 * 
 * @author lusine
 *
 */
public class Task_322 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_322 task = new Task_322();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray((getRequiredArray(array)));

	}

	/**
	 * Return a new array, add a two zero value after the 0-s element
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int[] requiredArray = new int[array.length + 2 * getCountZeroElements(array)];
		int j = 0;

		for (int i = 0; i < array.length; i++) {
			requiredArray[j++] = array[i];
			if (array[i] == 0) {
				requiredArray[j++] = 0;
				requiredArray[j++] = 0;
			}

		}
		return requiredArray;
	}

}
