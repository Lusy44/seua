package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the product of positive elements in array, which there is before last
 * min. If the min is first element, then return NUMBER.
 * 
 * @author lusine
 *
 */
public class Task_346 extends TaskArrayBase {
	private static final int NUMBER = 0;

	public static void main(String[] args) {
		Task_346 task = new Task_346();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("The product = " + getRequiredProduct(array));

	}

	/**
	 * * Return the product of positive elements in array, which there is before
	 * last min. If the min is first element, then return NUMBER.
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredProduct(int[] array) {
		int index = getLastMinIndex(array);

		if (index == 0) {
			return NUMBER;
		}

		return getProductBeforeLastMin(array, index);
	}

	/**
	 * Return last index Min element of array
	 * 
	 * @param array
	 * @return
	 */
	private int getLastMinIndex(int[] array) {
		int min = array[0];
		int index = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] <= min) {
				min = array[i];
				index = i;
			}
		}

		return index;
	}

	/**
	 * Return the product of positive elements in array, which there is before last
	 * min
	 * 
	 * @param array
	 * @return
	 */
	private int getProductBeforeLastMin(int[] array, int indexOfLastMax) {
		int product = 1;

		for (int i = indexOfLastMax - 1; i >= 0; i--) {
			if (array[i] > 0) {
				product *= array[i];
			}

		}
		return product;
	}

}
