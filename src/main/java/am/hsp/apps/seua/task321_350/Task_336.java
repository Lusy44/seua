package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the max element of array, if there is at least once negative element,
 * otherwise Print s = 0.
 * 
 * @author lusine
 *
 */
public class Task_336 extends TaskArrayBase {
	private static final int NUMBER = 0;

	public static void main(String[] args) {
		Task_336 task = new Task_336();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result " + getResult(array));
	}

	/**
	 * Return the max element of array, if there is at least once negative element,
	 * otherwise Return NUMBER.
	 * 
	 * @param array
	 * @return
	 */
	private int getResult(int[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] < 0) {
				return getMax(array);
			}
		}
		return NUMBER;
	}
}
