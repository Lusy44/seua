package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum of elements, which previous and next elements sum to less than
 * the given number
 * 
 * @author lusine
 *
 */
public class Task_333 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_333 task = new Task_333();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("The sum = " + getRequiredSum(array, number));

	}

	/**
	 * Return the sum of elements, which previous and next elements sum to less than
	 * the given number
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private int getRequiredSum(int[] array, int number) {
		int sum = 0;

		for (int i = 1; i < array.length - 1; i++) {
			if (isSumOfLastTwolessThanFirst(number, array[i - 1], array[i + 1])) {
				sum += array[i];
			}
		}

		return sum;
	}

	/**
	 * Check sum of the two numbers to less, than the given number
	 * 
	 * @param number
	 * @param number1
	 * @param number2
	 * @return
	 */
	private boolean isSumOfLastTwolessThanFirst(int number, int number1, int number2) {
		return (number1 + number2 < number);
	}
}
