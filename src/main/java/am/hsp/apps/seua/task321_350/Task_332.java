package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number n and the array [n] ... Create and print a new array which
 * elements are the elements of the array, put the square of min in the place of
 * the maximal element and put the cube max of the min element
 * 
 * @author lusine
 *
 */
public class Task_332 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_332 task = new Task_332();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		printArray((getRequiredArray(array)));
	}

	/**
	 * Return a new array which elements are the elements of the array, put the
	 * square of min in the place of the maximal element and put the cube max of the
	 * min element
	 * 
	 * @param array
	 * @return
	 */
	private int[] getRequiredArray(int[] array) {
		int[] requiredArray = new int[array.length];
		int max = getMax(array);
		int min = getMin(array);
		
		for (int i = 0; i < array.length; i++) {
	     
			if (array[i] == max) {
				requiredArray[i] = getPow(min, 2); 
				continue;
			}
			if (array[i] == min) {
				requiredArray[i] = getPow(max, 3);
				continue;
			}
			
			requiredArray[i] = array[i];
		}
		
		return requiredArray;
	}

}
