package main.java.am.hsp.apps.seua.task321_350;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the max element from index of the multiple (multiple = 3).
 * 
 * @author lusine
 *
 */
public class Task_339 extends TaskArrayBase {
	private static final int MULTIPLE = 3;

	public static void main(String[] args) {
		Task_339 task = new Task_339();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result " + getRequiredMax(array, MULTIPLE));

	}

	/**
	 * Return the max element from index of the multiple
	 * 
	 * @param array
	 * @param divisor
	 * @return
	 */
	private int getRequiredMax(int[] array, int multiple) {
		int max = array[3];

		for (int i = multiple; i < array.length; i += multiple) {
			if (array[i] > max) {
				max = array[i];
			}
			
		}
    
		return max;

	}
}
