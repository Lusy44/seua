package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Print,sum of the given number's digits value divided by three digit
 * number, if Ones is greater than the Tens Digit, otherwise Print the three digit
 * number
 * 
 * @author lusine
 *
 */
public class Task_56 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_56 task = new Task_56();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter three digit number");

		System.out.println("Result = " + getResult(number));
	}

	/**
	 * Check and Return the sum of its digits value divided by three digit number,
	 * if Ones greater than the Tens Digit, otherwise Return the three digit number
	 * 
	 * @param number
	 * @return
	 */
	private double getResult(int number) {
		if (getOnes(number) > getTens(number)) {
			return division(getSumOfDigit(number), number);
		}
		return number;

	}

}
