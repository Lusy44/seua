package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Print, Tens Digit divided by Ones Digit, if the given three
 * digit number greater than REQ_VALUE, otherwise Print Hundreds Digit divided
 * by Ones Digit
 * 
 * @author lusine
 *
 */
public class Task_57 extends TaskDigitBase {
	private static final int REQ_VALUE = 300;

	public static void main(String[] args) {
		Task_57 task = new Task_57();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter three digit number");
		System.out.println("Result = " + getResult(number));

	}

	/**
	 * Return, Tens Digit divided by Ones Digit, if the given three digit number
	 * greater than REQ_VALUE, otherwise Return Hundreds Digit divided by Ones
	 * 
	 * @param number
	 * @return
	 */
	private double getResult(int number) {
		if (number > REQ_VALUE) {
			return division(getTens(number), getOnes(number));
		}
		return division(getHundreds(number), getOnes(number));

	}

}
