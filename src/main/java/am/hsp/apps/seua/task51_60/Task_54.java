package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Return the Maximum digit of the given three digit number
 * 
 * @author lusine
 *
 */
public class Task_54 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_54 task = new Task_54();
		task.solve();
  
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");
		System.out.println("Result = " + getMaxDigit(number));

	}
   
}
