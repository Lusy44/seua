package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "true" if there are equal digits in the given Three digit number,
 * otherwise Print "false"
 * 
 * @author lusine
 *
 */
public class Task_52 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_52 task = new Task_52();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");
		System.out.println("Relust:  " + hasEqualDigits(number));
	}

	/**
	 * Return "true", if there are equal digits, otherwise print "false"
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	private boolean hasEqualDigits(int number) {
		int ones = getOnes(number);
		int tens = getTens(number);
		int hundreds = getHundreds(number);

		return (ones == tens || ones == hundreds || tens == hundreds);
	}

}
