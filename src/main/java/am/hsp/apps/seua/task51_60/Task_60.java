package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Sort and Print the digits in descending order
 * 
 * @author lusine
 *
 */
public class Task_60 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_60 task = new Task_60();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");

		System.out.println(getDescendingOrder(number));

	}

	/**
	 * Return sort the digits in  descending order
	 * 
	 * @param number
	 */
	private String getDescendingOrder(int number) {
		return (getMaxDigit(number) + ", " + getMid(getOnes(number), getTens(number), getHundreds(number))
		+ ", " + getMinDigit(number));
	}

}
