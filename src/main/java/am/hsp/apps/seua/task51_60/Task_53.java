package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Print, a three digit number divided by the sum of its digits
 * value, if the number is greater than given "k" number, otherwise Print
 * Ones divided by the three digit number
 * 
 * @author lusine
 *
 */
public class Task_53 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_53 task = new Task_53();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");
		int givenNumber = getUserInput("Please enter given number");
		
		System.out.println("Result = " + getResult(number, givenNumber));
		
	}

	/**
	 * Check and return, a three digit number divided by the sum of its digits
	 * value, if three digit number greater than the given number k, otherwise a
	 * Ones divided by the three digit number
	 */
	private double getResult(int number, int givenNumber) {
		if (number > givenNumber) {
			return division(number, getSumOfDigit(number));
		}
		return division(getOnes(number), number);

	}

}
