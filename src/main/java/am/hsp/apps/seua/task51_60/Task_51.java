package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print "true", if three digit number's Ones digit is equal to the sum of Tens
 * digit and Hundreds digit, Otherwise Print "false".
 * 
 * @author Lusine
 *
 */
public class Task_51 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_51 task = new Task_51();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");
		System.out.println(isOnesEqualSumTensHundreds(number));
	//	System.out.println(removeLastDigit(number) + "  " +removeLastDigit(number));
    	System.out.println("count"+getDigitCount(number));

	}

	/**
	 * Return "true", if Ones Digit equal of the Tens + Hundreds, otherwise "false"
	 * 
	 * @param number
	 * @return
	 */
	private boolean isOnesEqualSumTensHundreds(int number) {
		return (getOnes(number) == getHundreds(number) + getTens(number));
	}

}
