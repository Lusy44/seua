package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Print ANSWER_A, if Tens Digit and Ones Digit the sum is less than REQ_VALUE, otherwise Print ANSWER_B
 * 
 * @author lusine
 */
public class Task_58 extends TaskDigitBase {
	private static final char ANSWER_A = 'a';
	private static final char ANSWER_B = 'b';
	
	private static final int REQ_VALUE = 5;

	public static void main(String[] args) {
		Task_58 task = new Task_58();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");
		System.out.println("Result:  " + getResult(number));

	}

	/**
	 * Return ANSWER_A, if Tens Digit and Ones Digit the sum is less than REQ_VALUE, otherwise Return ANSWER_B
	 * 
	 * @param number
	 * @return
	 */
	private char getResult(int number) {
		if (getTens(number) + getOnes(number) < REQ_VALUE) {
			return ANSWER_A;
		}
		return ANSWER_B;
	}
}
