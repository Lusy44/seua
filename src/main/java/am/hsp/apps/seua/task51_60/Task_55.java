package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Calculate and Return the Minimum digit of the given three digit number
 * 
 * @author lusine
 * 
 */
public class Task_55 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_55 task = new Task_55();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter three digit number");

		System.out.println("Result = " + getMinDigit(number));

	}

}
