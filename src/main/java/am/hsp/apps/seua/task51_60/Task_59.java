package main.java.am.hsp.apps.seua.task51_60;

import main.java.am.hsp.apps.seua.base.TaskDigitBase;

/**
 * Sort and Print the digits in ascending order
 * 
 * @author lusine
 *
 */
public class Task_59 extends TaskDigitBase {

	public static void main(String[] args) {
		Task_59 task = new Task_59();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number = getUserInput("Please enter the three digit number");

		System.out.println(getAscendingOrder(number));

	}

	/**
	 * Return sort the digits in ascending order
	 * 
	 * @param number
	 */
	private String getAscendingOrder(int number) {
		return (getMinDigit(number) + ", " + getMid(getOnes(number), getTens(number), getHundreds(number))
				+ ", " + getMaxDigit(number));
	}
}
