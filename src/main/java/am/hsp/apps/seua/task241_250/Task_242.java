package main.java.am.hsp.apps.seua.task241_250;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the product of Array elements, which divide by number without remainder
 * 
 * @author lusine
 *
 */
public class Task_242 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_242 task = new Task_242();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		int number = getUserInput("Please enter number");
		
		int[] array = createArray(size, startRange, endRange);
		

		printArray(array);
		System.out.println("The Product = " + getProductComplyTheCondition(array, number));
	}

	/**
	 * Return the product of Array elements, which divide by number without remainder
	 * 
	 * @param array
	 */
	private int getProductComplyTheCondition(int[] array, int divisor) {
		int product = 1;
	
		for (int i = 0; i < array.length; i++) {
			if (isMultiple(array[i], divisor)) {
				product *= array[i];
			}
		}
		return product;
	}
}