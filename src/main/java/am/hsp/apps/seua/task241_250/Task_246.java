package main.java.am.hsp.apps.seua.task241_250;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the arithmetic average of the elements, in which the index is the square of the number
 * 
 * @author lusine
 *
 */
public class Task_246 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_246 task = new Task_246();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		
		int[] array = createArray(size, startRange, endRange);
		

		printArray(array);
		System.out.println("The arithmetic average = " + getRequiredAverage(array));
	}

	/**
	 * Return the arithmetic average of the elements, in which the index is the square of the number
	 * 
	 * @param array
	 */
	private double getRequiredAverage(int[] array) {
		int sum = 0;
		int count = 0;
	
		for (int i = 0; i < array.length; i++) {
			if (hasRoot(i)) {
				sum += array[i];
				count++;
			}
		}
		return (double)sum / count;
	}
}