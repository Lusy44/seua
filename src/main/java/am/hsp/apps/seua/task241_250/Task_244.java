package main.java.am.hsp.apps.seua.task241_250;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the product of Array elements, that are divided into "DIVISOR" and receive "REMAINDER" balances
 * 
 * @author lusine
 *
 */
public class Task_244 extends TaskArrayBase {
	private static final int DIVISOR = 5;
	private static final int REMAINDER = 2;
	
	public static void main(String[] args) {
		Task_244 task = new Task_244();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		
		int[] array = createArray(size, startRange, endRange);
		

		printArray(array);
		System.out.println("The Product = " + getProductComplyTheCondition(array, DIVISOR, REMAINDER));
	}

	/**
	 * Return the product of Array elements, that are divided into "DIVISOR" and receive "REMAINDER" balances
	 * 
	 * @param array
	 */
	private int getProductComplyTheCondition(int[] array, int divisor, int remainder) {
		int product = 1;
	
		for (int i = 0; i < array.length; i++) {
			if (isMultipleWithRemainder(array[i], divisor, remainder)) {
				product *= array[i];
			}
		}
		return product;
	}
}