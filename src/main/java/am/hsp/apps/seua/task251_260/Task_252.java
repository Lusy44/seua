package main.java.am.hsp.apps.seua.task251_260;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print Min element of Array
 * 
 * @author lusine
 *
 */
public class Task_252 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_252 task = new Task_252();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Mix element = " + getMin(array));

	}

}
