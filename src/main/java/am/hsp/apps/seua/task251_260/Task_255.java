package main.java.am.hsp.apps.seua.task251_260;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the sum of the Max element and index in the array
 * 
 * @author lusine
 *
 */
public class Task_255 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_255 task = new Task_255();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);

		System.out.println("The sum = " + getSumOfElmAndIndex(array));

	}

	/**
	 * Return the sum of the max element and the index in the array
	 * 
	 * @param array
	 * @return
	 */
	private int getSumOfElmAndIndex(int[] array) {
		int max = array[0];
		int index = 0;
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
				index = i;
			}
		}

		return max + index;
	}

}
