package main.java.am.hsp.apps.seua.task251_260;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print Sum of Max And Min Elements
 * 
 * @author lusine
 *
 */
public class Task_253 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_253 task = new Task_253();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);

		 System.out.println("The sum = " + (getSumOfMaxAndMin(array)));

	}

	private int getSumOfMaxAndMin(int[] array) {
		int max = array[0];
		int min = array[0];

		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
			if (array[i] < min) {
				min = array[i];
			}
		}

		return max + min;
	}

}
