package main.java.am.hsp.apps.seua.task251_260;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print Max element of Array
 * 
 * @author lusine
 *
 */
public class Task_251 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_251 task = new Task_251();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Max element = " + getMax(array));

	}

}
