package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the
 * arithmetic average the elements which sum of indexes is even and their are
 * above the main diagonal or on the main diagonal.
 * 
 * @author lusine
 *
 */
public class Task_448 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_448 task = new Task_448();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int[][] twoDimensionalArray = { { 11, 15, 20, 30 }, 
										{ 12, 11, -10, -20 },
										{ -17, 7, 18, 5 },
										{ -27, 7, 16, 4 } };

		System.out.println("The arithmetic average = " + getRequiredArithmeticAvg(twoDimensionalArray));
	}

	/**
	 * Return the arithmetic average the elements which sum of indexes is even and
	 * their are above the main diagonal or on the main diagonal.
	 * 
	 * @param array
	 * @return
	 */
	private double getRequiredArithmeticAvg(int[][] array) {
		int sum = 0;
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) {
				if (isEven(i + j)) {
					sum += array[i][j];
					count++;
				}
			}
		}

		return division(sum, count);
	}

}
