package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the
 * arithmetic average of the elements which from a to b([a:b]) and below the
 * main diagonal
 *
 * @author lusine
 *
 */
public class Task_436 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_436 task = new Task_436();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int beginRange = getUserInput("Please enter begin A of range");
		int finishRange = getUserInput("Please enter finish B of range");

		int[][] twoDimensionalArray = { { 10, 15, 20, 30 }, 
										{ 15, 11, -10, -20 },
										{ -10, 7, 18, 5 },
										{ -10, 7, 16, 4 } };

		printTwoDimensionalArray(twoDimensionalArray);
		System.out.println(
				"The arithmetic average = " + getRequiredArithmeticAvg(twoDimensionalArray, beginRange, finishRange));
	}

	/**
	 * Return the arithmetic average of the elements which from a to b([a:b]) and
	 * below the main diagonal
	 * 
	 * @param array
	 * @param begin
	 * @param end
	 * @return
	 */
	private double getRequiredArithmeticAvg(int[][] array, int begin, int end) {
		int sum = 0;
		int count = 0;

		for (int i = 1; i < array.length; i++) {
			for (int j = 0; j < i; j++) {
				if (isNumberOfRange(array[i][j], begin, end)) {
					sum += array[i][j];
					count++;
				}
			}
		}

		return division(sum, count);
	}



}
