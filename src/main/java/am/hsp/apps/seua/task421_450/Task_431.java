package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the count of
 * the element which sum in the indexes odd and on the auxiliary diagonal or
 * below the auxiliary diagonal.
 * 
 * @author lusine
 *
 */
public class Task_431 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_431 task = new Task_431();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int[][] twoDimensionalArray = { { 10, 15, 20, 30 }, 														
										{ 15, 11, -10, -20 }, 
										{ -10, 7, 18, 5 },
										{ -10, 7, 18, 5 } };

		printTwoDimensionalArray(twoDimensionalArray);
		System.out.println("The count = " + getCountElmSumOfIndexOdd(twoDimensionalArray));
	}

	/**
	 * Return the count of the element which sum in the indexes odd and on the
	 * auxiliary diagonal or below the auxiliary diagonal.
	 * 
	 * @param array
	 * @return
	 */
	private int getCountElmSumOfIndexOdd(int[][] array) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = array.length - 1 - i; j < array.length; j++) {
				if (isOdd(i + j)) {
					count++;
				}
			}
		}

		return count;
	}

}
