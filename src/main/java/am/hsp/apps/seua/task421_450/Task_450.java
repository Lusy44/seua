package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the
 * fractional part sum of the elements which are below the auxiliary diagonal.
 * 
 * @author lusine
 *
 */
public class Task_450 extends TaskArrayBase {

    public static void main(String[] args) {
        Task_450 task = new Task_450();
        task.solve();

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
     */
    @Override
    public void solve() {
        double[][] twoDimensionalArray = { { 13.25, 23.25, 3.25 },
                                           { 15.13, 11.47, 10.45 }, 
                                           { 10.16, 27.11, 12.15 } };

        System.out.println("The fractional part = " + getRequiredSum(twoDimensionalArray));

    }

    /**
     * Return the fractional part sum of the elements which are below the auxiliary
     * diagonal.
     * 
     * @param array
     * @return
     */
    private double getRequiredSum(double[][] array) {
        double sum = 0;

        for (int i = 1; i < array.length; i++) {
            for (int j = array.length - i; j < array.length; j++) {
                sum += getFractionalPartFromNumber(array[i][j]);
            }
        }

        return sum;
    }

    /**
     * Return the fractional part of the given number
     * 
     * @param number
     * @return
     */
    private double getFractionalPartFromNumber(double number) {
        return number - (int) number;
    }

}
