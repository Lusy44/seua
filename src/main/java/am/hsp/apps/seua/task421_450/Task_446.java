package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the sum of
 * the elements which from a to b([a:b]) and above the main diagonal or on the
 * main diagonal.
 * 
 * @author lusine
 *
 */
public class Task_446 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_446 task = new Task_446();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int beginRange = getUserInput("Please enter begin A of range");
		int finishRange = getUserInput("Please enter finish B of range");

		int[][] twoDimensionalArray = { { 10, 15, 20, 30 }, { 15, 11, -10, -20 }, { -10, 7, 18, 5 },
				{ -10, 7, 16, 4 } };
		System.out.println(getRequiredArithmeticAvg(twoDimensionalArray, beginRange, finishRange));

	}

	/**
	 * Return the sum of the elements which from a to b([a:b]) and above the main
	 * diagonal or on the main diagonal
	 * 
	 * @param array
	 * @param begin
	 * @param end
	 * @return
	 */
	private double getRequiredArithmeticAvg(int[][] array, int begin, int end) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) {
				if (isNumberOfRange(array[i][j], begin, end)) {
					sum += array[i][j];
				}
			}
		}

		return sum;
	}

}
