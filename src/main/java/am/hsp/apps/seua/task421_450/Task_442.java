package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;
/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print arithmetic average of the
 * negative elements, which above the auxiliary diagonal.
 * @author lusine
 *
 */
public class Task_442 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_442 task = new Task_442();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int[][] twoDimensionalArrayonalArray = { { 10, 15, 20, 30, 10 }, 
												 { -7, 11, -10, -20, 8 },
				                                 { -10, 7, 18, 5, -10 },
				                                 { -20, 4, 6, 4, -11},
				                                 { -10, 7, 16, 4, -11}};
		
		printTwoDimensionalArray(twoDimensionalArrayonalArray);
		System.out.println("The Arithmetic average = " + getRequiredArithmeticAvg(twoDimensionalArrayonalArray));
	}

	/**
	 * Return arithmetic average of the negative elements, which above the auxiliary
	 * diagonal.
	 * 
	 * @param array
	 * @return
	 */
	private double getRequiredArithmeticAvg(int[][] array) {
		int sum = 0;
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length - 1 - i; j++) {
				if (array[i][j] < 0) {
					sum += array[i][j];
					count++;
				}
			}
		}

		return division(sum, count);

	}
}
