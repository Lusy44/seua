package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print out the sum
 * of the even elements above the auxiliary diagonal.
 * 
 * @author lusine
 *
 */
public class Task_426 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_426 task = new Task_426();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[][] twoDimensionalArray = createTwoDimensionalArray(size, startRange, endRange);

		printTwoDimensionalArray(twoDimensionalArray);
		System.out.println(getRequiredSum(twoDimensionalArray));
	}

	/**
	 * Return out the sum of the even elements above the auxiliary diagonal
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredSum(int[][] array) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length - 1 - i; j++) {
				if (isEven(array[i][j])) {
					sum += array[i][j];
				}
			}
		}
		
		return sum;
	}

}
