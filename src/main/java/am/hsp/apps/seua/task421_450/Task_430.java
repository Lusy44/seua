package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the
 * arithmetic average of the even elements on the main diagonal or below the
 * main diagonal.
 * 
 * @author lusine
 *
 */
public class Task_430 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_430 task = new Task_430();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[][] twoDimensionalArray = createTwoDimensionalArray(size, startRange, endRange);

		printTwoDimensionalArray(twoDimensionalArray);
		System.out.println("The arithmetic average = " + getRequiredArithmeticAvg(twoDimensionalArray));
	}

	/**
	 * Return the arithmetic average of the even elements on the main diagonal or
	 * below the main diagonal.
	 * 
	 * @param array
	 * @return
	 */
	private double getRequiredArithmeticAvg(int[][] array) {
		int sum = 0;
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j <= i; j++) {
				if (isEven(array[i][j])) {
					sum += array[i][j];
					count++;
				}
			}
		}

		return division(sum, count);
	}

}
