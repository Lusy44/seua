package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print the product
 * of the elements, which above the main diagonal and their sum of indexes is
 * odd.
 * 
 * @author lusine
 *
 */
public class Task_439 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_439 task = new Task_439();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[][] twoDimensionalArray = createTwoDimensionalArray(size, startRange, endRange);

		printTwoDimensionalArray(twoDimensionalArray);
		System.out.println("The product = " + getRequiredProduct(twoDimensionalArray));

	}

	/**
	 * Return the product of the elements, which above the main diagonal and their
	 * sum of indexes is odd
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredProduct(int[][] array) {
		int product = 1;

		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (isOdd(i + j)) {
					product *= array[i][j];
				}
			}
		}

		return product;
	}

}
