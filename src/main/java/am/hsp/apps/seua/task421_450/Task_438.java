package main.java.am.hsp.apps.seua.task421_450;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Given the number m and the Two Dimensional Array[m][m] ... Print count of the
 * positive elements, which below the main diagonal and are in pairs of columns.
 * 
 * @author lusine
 *
 */
public class Task_438 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_438 task = new Task_438();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int[][] twoDimensionalArray = { { 10, 15, 20, 30, 10 }, 
				  						{ 15, 11, -10, -20, 8 },
				  						{ -10, 7, 18, 5, -10 },
				  						{ -10, 4, 6, 4, -11 },
				  						{ -10, 7, 16, 4, -11 } };

		printTwoDimensionalArray((twoDimensionalArray));
		System.out.println("The count = " + getRequiredCount(twoDimensionalArray));
	}

	/**
	 * 
	 * Return count of the positive elements, which below the main diagonal and are
	 * in pairs of columns(exp. j=1,3... because j is pairs number columns).
	 * 
	 * @param array
	 * @return
	 */
	private int getRequiredCount(int[][] array) {
		int count = 0;

		for (int i = 1; i < array.length; i++) {
			for (int j = 1; j < i; j += 2) {
				if (array[i][j] > 0) {
					count++;
				}
			}
		}

		return count;
	}

}
