package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print absolute sum of elements of odd index elements from array
 * 
 * @author lusine
 *
 */
public class Task_218 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_218 task = new Task_218();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Absolute sum of odd index " + getAbsSumOfOddIndexElements(array));
	}

	/**
	 * Calculate Absolute sum of odd index elements
	 * 
	 * @param array
	 * @return
	 */
	private int getAbsSumOfOddIndexElements(int[] array) {
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (isOdd(i)) {
				sum += Math.abs(array[i]);
			}
		}

		return sum;
	}

}
