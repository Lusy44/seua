package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print count index, which multiple divisor
 *
 * 
 * @author lusine
 *
 */
public class Task_219 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_219 task = new Task_219();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		int divisor = getUserInput("Please enter divisor");
		
		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Result: " + getCountIndexByMultiple(array, divisor));
	}

	/**
	 * Return count index, which multiple divisor 
	 * 
	 * @param array
	 * @return
	 */
	private int getCountIndexByMultiple(int[] array, int divisor) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (isMultiple(i, divisor)) {
				count++;
			}
		}
        
		return count;
	}

}
