package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print average square of positive elements
 * 
 * @author lusine
 *
 */
public class Task_212 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_212 task = new Task_212();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Average Square " + getAverageSquare(array));
	}

	/**
	 * Calculate average square of positive elements
	 * 
	 * @param array
	 * @return
	 */
	private double getAverageSquare(int[] array) {
		int count = 0;
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] > 0) {
				sum += getPow(array[i], 2);
				count++;
			}
		}
		
		return (double)sum / count;
	}

}
