package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print arithmetic average of positive elements
 * 
 * @author lusine
 *
 */
public class Task_211 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_211 task = new Task_211();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Arithmetic average " + getArithmeticAverage(array));
	}

	/**
	 * Calculate arithmetic average of positive elements
	 * @param array
	 * @return
	 */
	private double getArithmeticAverage(int[] array) {
		int count = 0;
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] > 0) {
				sum += array[i];
				count++;
			}
		}
		
		return (double)sum / count;
	}

}
