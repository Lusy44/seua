package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print multiple of elements of even index from an array
 * 
 * @author lusine
 *
 */
public class Task_216 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_216 task = new Task_216();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");

		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("Product of even index " + getProductOfEvenIndexElements(array));
	}

	/**
	 * Calculate product of even index
	 * 
	 * @param array
	 * @return
	 */
	private int getProductOfEvenIndexElements(int[] array) {
		int product = 1;

		for (int i = 0; i < array.length; i++) {
			if (isEven(i)) {
				product *= array[i];
			}
		}

		return product;
	}

}
