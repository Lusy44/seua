package main.java.am.hsp.apps.seua.task211_220;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Print the count of positive and negative elements
 *
 * 
 * @author lusine
 *
 */
public class Task_220 extends TaskArrayBase {

	public static void main(String[] args) {
		Task_220 task = new Task_220();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int startRange = getUserInput("Please enter start of range");
		int endRange = getUserInput("Please enter end of range");
		int size = getUserInput("Please enter size of array");
		
		int[] array = createArray(size, startRange, endRange);

		printArray(array);
		System.out.println("The count positive elements: " + getCountPositiveElements(array));
		System.out.println("The count positive elements: " + getCountNegativeElements(array));
	}

	

}
