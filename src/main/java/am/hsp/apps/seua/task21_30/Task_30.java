package main.java.am.hsp.apps.seua.task21_30;

import java.util.Scanner;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Sort the numbers in descending order
 */
public class Task_30 extends TaskBase {

	public static void main(String[] args) {
		Task_30 task = new Task_30();
		task.solve();

	}
	/*
	 * (non-Javadoc)
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("PLease, enter number1");
		int number2 = getUserInput("PLease, enter number2");
		int number3 = getUserInput("PLease, enter number3");

		System.out.println("Result " + getMax(number1, number2, number3) + ", "
				+ getMid(number1, number2, number3) + ", " + getMin(number1, number2, number3));
	}
 

	
}