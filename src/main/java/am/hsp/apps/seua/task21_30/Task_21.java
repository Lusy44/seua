package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the maximum of given 3 integers
 */
public class Task_21 extends TaskBase {

	public static void main(String[] args) {
		Task_21 task = new Task_21();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: Max = " + getMax(number1, number2));
	}

}
