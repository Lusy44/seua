package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print 1, if at least one number is even, else 2
 */
public class Task_26 extends TaskBase {

	public static void main(String[] args) {
		Task_26 task = new Task_26();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int number1 = getUserInput("Please enter number 1");
		int number2 = getUserInput("Please enter number 2");
		int number3 = getUserInput("Please enter number 3");

		System.out.println("Result y=" + (isAnyEven(number1, number2, number3) ? 1 : 2));

	}

	/**
	 * Return "true", if at least one number is even, otherwise Return "false";
	 */
	private boolean isAnyEven(int number1, int number2, int number3) {
		return (isEven(number1) || isEven(number2) || isEven(number3));
	}

}
