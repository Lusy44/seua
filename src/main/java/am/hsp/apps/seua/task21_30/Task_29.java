package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Sort the numbers in ascending order
 */
public class Task_29 extends TaskBase {

	public static void main(String[] args) {

		Task_29 task = new Task_29();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("PLease, enter number1");
		int number2 = getUserInput("PLease, enter number2");
		int number3 = getUserInput("PLease, enter number3");

		System.out.println("Result " + getMin(number1, number2, number3) + ", " + getMid(number1, number2, number3) + ", "
				+ getMax(number1, number2, number3));

	}

}
