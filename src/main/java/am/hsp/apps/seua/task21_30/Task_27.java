package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true" , if the given Numbers are arithmetic progression, otherwise
 * Print "false"
 */

public class Task_27 extends TaskBase {

	public static void main(String[] args) {
		Task_27 task = new Task_27();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: " + isArithmeticProgression(number1, number2, number3));

	}

}
