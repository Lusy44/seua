package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true" if the given Numbers are geometric progression, otherwise
 * "false"
 */
public class Task_28 extends TaskBase {
	public static void main(String[] args) {
		Task_28 task = new Task_28();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: " + isGeometricProgression(number1, number2, number3));

	}

}
