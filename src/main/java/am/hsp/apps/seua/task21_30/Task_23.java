package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true" if any of the numbers is equal to request final, otherwise print
 * "false"
 * 
 * @author lusine
 */
public class Task_23 extends TaskBase {
	private static final int REQ_VALUE = 1;

	public static void main(String[] args) {
		Task_23 task = new Task_23();
		task.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Plaese enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: " + isAnyNumberEqualToRequiredValue(number1, number2, number3));

	}

	/**
	 * Return "true", if the number equal to Required Value, otherwise Return
	 * "false"
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	private boolean isNumberEqualToRequiredValue(int number1) {
		return (number1 == REQ_VALUE);
	}

	/**
	 * Return "true", if there is ANY number equal to Required Value, otherwise
	 * Return "false"
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	private boolean isAnyNumberEqualToRequiredValue(int number1, int number2, int number3) {
		return (isNumberEqualToRequiredValue(number1) || isNumberEqualToRequiredValue(number2)
				|| isNumberEqualToRequiredValue(number3));
	}

}
