package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print "true", if only two numbers equal required value, otherwise Print
 * "false"
 * 
 * @author lusine
 *
 */
public class Task_24 extends TaskBase {
	private static final int REQ_VALUE = 2;

	public static void main(String[] args) {

		Task_24 task = new Task_24();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int number1 = getUserInput("Please enter number1");
		int number2 = getUserInput("Please enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: " + isAnyTwoParamEqualReqValue(number1, number2, number3));

	}

	/**
	 * Two numbers is Equal 2 and 3th number is not equal
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	private static boolean isOnlyFirstTwoEqualReqValue(int number1, int number2, int number3) {
		return ((number1 == REQ_VALUE && number2 == REQ_VALUE && number3 != REQ_VALUE));
	}

	/**
	 * Check there are two number equal to the required value, then Return "true",
	 * otherwise "false"
	 * 
	 * @param number1
	 * @param number2
	 * @param number3
	 * @return
	 */
	private static boolean isAnyTwoParamEqualReqValue(int number1, int number2, int number3) {
		return (isOnlyFirstTwoEqualReqValue(number1, number2, number3)
				|| isOnlyFirstTwoEqualReqValue(number3, number1, number2)
				|| isOnlyFirstTwoEqualReqValue(number3, number2, number1));
	}
}
