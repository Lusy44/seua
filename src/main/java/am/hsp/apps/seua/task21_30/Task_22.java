package main.java.am.hsp.apps.seua.task21_30;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print the minimum of given 3 numbers
 * 
 * @author lusine
 *
 */
public class Task_22 extends TaskBase {
	public static void main(String[] args) {

		Task_22 task = new Task_22();
		task.solve();
	}

	@Override
	public void solve() {

		int number1 = getUserInput("PLease enter number1");
		int number2 = getUserInput("PLease enter number2");
		int number3 = getUserInput("Please enter number3");

		System.out.println("Result: Min = " + getMin(number1, number2, number3));
	}

}
