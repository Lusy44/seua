package main.java.am.hsp.apps.seua.task21_30;

import java.util.Scanner;

import main.java.am.hsp.apps.seua.base.TaskBase;

/**
 * Print y=1, if you can build a triangle by the given sides, else print y=2;
 * 
 * @author lusine
 *
 */
public class Task_25 extends TaskBase {
	public static void main(String[] args) {

		Task_25 task = new Task_25();
		task.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {

		int side1 = getUserInput("Please enter side1");
		int side2 = getUserInput("Please enter side2");
		int side3 = getUserInput("Please enter side3");

		System.out.println("Result:y= " + (isTriangle(side1, side2, side3)));

	}

	/**
	 * Check first number is small than two other numbers sum then Return "true",
	 * otherwise "fasle"
	 * 
	 * @param side1
	 * @param side2
	 * @param side3
	 * @return
	 */
	private static boolean isFirstNumberSmallSumTwoOther(int side1, int side2, int side3) {
		return (side1 < side2 + side3);
	}

	/**
	 * Can build a triangle
	 * 
	 * @param side1
	 * @param side2
	 * @param side3
	 * @return
	 */
	private static boolean isTriangle(int side1, int side2, int side3) {
		return (isFirstNumberSmallSumTwoOther(side1, side2, side3) && isFirstNumberSmallSumTwoOther(side2, side3, side1)
				&& isFirstNumberSmallSumTwoOther(side3, side2, side1));

	}

}
